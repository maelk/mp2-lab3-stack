// ������������ ��������� ������ - ������� ���������� �����

#ifndef _TSTACK_H_
#define _TSTACK_H_

#define MemSize 25 // ������ ������ ��� �����

class TSimpleStack {
	protected:
		int Mem[MemSize]; // ������ ��� ��
		int Top; // ������ ���������� �������� �������� � Mem - ������� �����
	public:
		TSimpleStack() { Top = -1; }
		bool IsEmpty() const { return Top == -1; } // �������� �������
		bool IsFull() const { return Top == MemSize - 1; } // �������� ������������
		void Push(const int Val) // �������� ��������
		{ 
			if (IsFull())
				throw("Stack is full");
			else
				Mem[++Top] = Val;
		} 
		int Pop() // ������� ��������
		{ 
			if (IsEmpty())
				throw("Stack is empty");
			else
				return Mem[Top--];
		}
};
#endif
