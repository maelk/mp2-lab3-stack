TEST(TStack, fill_up_to_a_maximum)
{
	const int size = 2;
	TStack A(size);
	A.Put(1);
	A.Put(1);
	EXPECT_EQ(A.IsFull(), true);
}

TEST(TStack, is_full_test)
{
	const int size = 2;
	TStack A(size);
	A.Put(1);
	EXPECT_NE(A.IsFull(), true);
}

TEST(TStack, is_empty_test_true)
{
	const int size = 2;
	TStack A(size);
	A.Put(1);
	A.Put(1);
	A.Pop();
	A.Pop();
	EXPECT_EQ(A.IsEmpty(), true);
}

TEST(TStack, is_empty_test_false)
{
	const int size = 2;
	TStack A(size);
	A.Put(1);
	A.Put(1);
	A.Pop();
	EXPECT_NE(A.IsEmpty(), true);
}


TEST(TStack, memory_is_empty_on_adding)
{
	TStack A(0);
	A.Put(1);
	EXPECT_EQ(DataNoMem, A.GetRetCode());
}

TEST(TStack, memory_is_empty_on_removing)
{
	TStack A(0);
	A.Get();
	EXPECT_EQ(DataNoMem, A.GetRetCode());
}

TEST(TStack, dataok_ret_code)
{
	const int size = 2;
	TStack A(size);
	A.Put(1);
	A.Put(1);
	EXPECT_EQ(DataOK, A.GetRetCode());
}

TEST(TStack, can_get_value)
{
	const int size = 2;
	TStack A(size);
	A.Put(1);
	A.Put(2);
	EXPECT_EQ(2, A.Get());
}

TEST(TStack, can_insert_element_in_full_stack)
{
	const int size = 2;
	TStack A(size);
	A.Put(1);
	A.Put(2);
	A.Put(3);
	EXPECT_EQ(3, A.Get());
}

TEST(TStack, can_get_code_return_is_empty)
{
	const int size = 2;
	TStack A(size);
	A.Get();
	EXPECT_EQ(DataEmpty, A.GetRetCode());
}