#ifndef __SIMPLESTACK_H__
#define __SIMPLESTACK_H__

#define MemSize   25  // ������ ����� �� ���������

template <class TypeData = int, int N = MemSize>
class TSimpleStack
{
private:
	int Top;
	TypeData Data[N];
public:
	TSimpleStack();
	void Push(const TypeData &Val);
	TypeData Pop();
	bool IsEmpty() const { return Top == -1; }
	bool IsFull() const { return Top == N-1; }
	void Print();
};

template <class TypeData, int N>
TSimpleStack <TypeData, N> ::TSimpleStack() : Top(-1)
{
}

template <class TypeData, int N>
void TSimpleStack <TypeData, N> ::Push(const TypeData &Val)
{
	if (IsFull()) throw "StackOverflow";
	else
	{
		Data[++Top] = Val;
	}
}

template <class TypeData, int N>
TypeData TSimpleStack <TypeData, N> ::Pop()
{
	TypeData result = -1;
	if (IsEmpty()) throw "StackIsEmpty";
	else
	{
		result = Data[Top--];
	}
	return result;
}

template <class TypeData, int N>
void TSimpleStack <TypeData, N> ::Print()
{
	for (int i = 0; i <= Top; i++)
		cout << Data[i] << ' ';
	cout << endl;
}

#endif