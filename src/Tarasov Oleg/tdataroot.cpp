#include "../include/tdataroot.h"

TDataRoot::TDataRoot(int Size) : TDataCom()
{
	DataCount = 0;
	MemSize = Size;
	if (Size == 0) {
		MemType = MEM_RENTER;
		pMem = nullptr;
	}
	else {
		MemType = MEM_HOLDER;
		pMem = new TElem[MemSize];
	}
}

TDataRoot::~TDataRoot() {
	if (MemType == MEM_HOLDER) delete[] pMem;
	pMem = nullptr;
}

void TDataRoot::SetMem(TElem *p, int Size) {
	int minSize = (MemSize < Size) ? MemSize : Size;
	MemSize = Size;
	pMem = new TElem[Size];
	for (int i = 0; i < minSize; i++) {
		pMem[i] = p[i];
	}
	if(DataCount > MemSize) // если размер уменьшается
		DataCount = MemSize;
	delete[] p;
}

bool TDataRoot::IsEmpty(void) const {
	return DataCount == 0;
}

bool TDataRoot::IsFull(void) const {
	return DataCount == MemSize;
}