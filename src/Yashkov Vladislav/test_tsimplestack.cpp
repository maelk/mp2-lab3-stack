#include "tsimplestack.h"
#include <gtest.h>

TEST(TSimpleStack, stack_is_empty)
{
	TSimpleStack<int> st;
	st.Put(1);
	st.Pop();
	EXPECT_TRUE(st.IsEmpty());
}
TEST(TSimpleStack, stack_is_full)
{
	TSimpleStack<int> st;
	for (int i = 0; i < MemSize; i++)
		st.Put(i);
	EXPECT_TRUE(st.IsFull());
}
TEST(TSimpleStack, extracted_element_is_equal_last_putted)
{
	TSimpleStack<int> st;
	st.Put(1);
	EXPECT_EQ(st.Pop(), 1);
}
TEST(TSimpleStack, cant_get_from_empty_stack)
{
	TSimpleStack<int> st;
	ASSERT_ANY_THROW(st.Pop());
}
TEST(TSimpleStack, can_pop_elem)
{
	TSimpleStack<int> st;
	st.Put(1);
	EXPECT_EQ(1, st.Pop());
}
TEST(TSimpleStack, cant_put_element_in_full_stack)
{
	TSimpleStack<int> st;
	for (int i = 0; i < MemSize; i++)
		st.Put(i);
	ASSERT_ANY_THROW(st.Put(0));
}