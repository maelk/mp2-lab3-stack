#include "TStack.h"
#include "calc_expr.h"
#include <string>

#include "gtest.h"

TEST(calc_expr, can_check_expression)
{
	string str = "(1+2)";
	EXPECT_TRUE(Expression_Check(str));
}

TEST(calc_expr, expression_checked_correctly)
{
	string str = "(1+2q)";
	EXPECT_FALSE(Expression_Check(str));
}

TEST(calc_expr, can_check_brackets)
{
	string str = "(1+2)";
	EXPECT_TRUE(Brackets_Check(str));
}

TEST(calc_expr, brackets_checked_correctly)
{
	string str = "(1+2";
	EXPECT_FALSE(Brackets_Check(str));
}

TEST(calc_expr, conversion_to_postfix_form_is_correctly)
{
	string str = "(1+2)/(3*4)-5";
	EXPECT_EQ("1 2 +3 4 */5 -", PostfixForm(str));
}

TEST(calc_expr, calculating_is_correctly)
{
	string str = "(1+2)/(3*4)-5";
	EXPECT_EQ(-4.75, Calculate(str));
}