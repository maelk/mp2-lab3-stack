#include "TStack.h"
#include "gtest.h"

TEST(TStack, created_stack_is_empty)
{
	TStack st;
	EXPECT_TRUE(st.IsEmpty());
}

TEST(TStack, can_put_in_stack)
{
	TStack st;
	st.Put(0);
	EXPECT_FALSE(st.IsEmpty());
}

TEST(TStack, can_get_from_stack)
{
	TStack st;
	st.Put(0);
	EXPECT_EQ(0, st.Get());
}


TEST(TStack, size_is_displayed_correctly)
{
	TStack st(45);
	EXPECT_EQ(45, st.Get_Size());
}

TEST(TStack, current_element_is_displayed_correctly)
{
	TStack st;
	st.Put(0);
	EXPECT_EQ(0, st.GetElem());
}

TEST(TStack, cant_get_from_empty_stack)
{
	TStack st;	
	EXPECT_ANY_THROW(st.Get());
}

TEST(TStack, can_get_from_full_stack)
{
	TStack st;
	for (int i = 0; i < st.Get_Size(); ++i)
		st.Put(0);
	st.Get();
	EXPECT_FALSE(st.IsFull());
}

TEST(TStack, can_put_in_full_stack)
{
	TStack st;	
	for (int i = 0; i < st.Get_Size(); ++i)
		st.Put(0);
	st.Put(0);
	EXPECT_NE(DefMemSize, st.Get_Size());
}

TEST(TStack, can_copy_stack)
{
	TStack st1;
	for (int i = 0; i < st1.Get_Size(); ++i)
		st1.Put(0);
	EXPECT_NO_THROW(TStack st2(st1));
}


TEST(TStack, copied_stack_has_its_own_memory)
{
	TStack st1;
	for (int i = 0; i < st1.Get_Size(); ++i)
		st1.Put(0);
	TStack st2(st1);
	EXPECT_NE(&st1, &st2);
}
