#include "TStack.h"
#include <iostream>
using namespace std;

TStack::TStack(const TStack& NewStack)
{
	MemSize = NewStack.MemSize;
	DataCount = NewStack.DataCount;
	MemType = NewStack.MemType;
	pMem = new TElem[MemSize];
	for (int i = 0; i < MemSize; ++i)
	{
		pMem[i] = NewStack.pMem[i];
	}
}

void TStack::Put(const TData& tmp)
{
	if (pMem == nullptr) 
	{ 
		throw SetRetCode(DataNoMem); 
	}
	else if (IsFull())
	{
		void* p = nullptr;
		SetMem(p, MemSize + DefMemSize);
		pMem[++Top] = tmp;
		DataCount++;
	}
	else
	{
		pMem[++Top] = tmp;
		DataCount++;
	}
}

TData TStack::Get(void)
{
	if (pMem == nullptr)	
		throw SetRetCode(DataNoMem);	
	else if (IsEmpty())	
		throw SetRetCode(DataEmpty);	
	else
	{
		DataCount--;
		return pMem[Top--];
	}
}

void TStack::Print()
{
	if (DataCount == 0) { cout << "Stack is empty!"; }
	for (int i = 0; i < DataCount; ++i)
	{
		cout << pMem[i] << " ";
	}
	cout << endl;
}

int  TStack::IsValid()
{
	int res = 0;
	if (pMem == nullptr)
		res++;
	if (MemSize < DataCount)
		res += 2;
	return res;
}
