#include "TSimpleStack.h"

#include "gtest.h"

TEST(TSimpleStack, created_stack_is_empty)
{
	TSimpleStack st;
	EXPECT_TRUE(st.IsEmpty());
}

TEST(TSimpleStack, can_pop_from_stack)
{
	TSimpleStact st;
	st.Push(0);
	EXPECT_EQ(0, st.Pop());
}
TEST(TSimpleStack, can_pop_from_full_stack)
{
	TSimpleStack st;
	for(int i = 0; i < DefMemSize; ++i)
		st.Push(0);
	st.Pop();
	EXPECT_FALSE(st.IsFull());
}

TEST(TSimpleStack, can_push_in_stack)
{
	TSimpleStack st;
	st.Push(5);
	EXPECT_FALSE(st.IsEmpty());
}

TEST(TSimpleStack, cant_pop_from_empty_stack)
{
	TSimpleStack st;	
	EXPECT_ANY_THROW(st.Pop());
}

TEST(TSimpleStack, cant_push_in_full_stack)
{
	TSimpleStack st;
	for (int i = 0; i < DefMemSize; ++i)
		st.Push(0);
	EXPECT_ANY_THROW(st.Push(0));
}

TEST(TSimpleStack, can_copy_stack)
{
	TSimpleStack st1;
	for (int i = 0; i < DefMemSize; ++i)
		st1.Push(0);
	EXPECT_NO_THROW(TSimpleStack st2(st1));
}


TEST(TSimpleStack, copied_stack_has_its_own_memory)
{
	TSimpleStack st1;
	for (int i = 0; i < DefMemSize; ++i)
		st1.Push(0);
	TSimpleStack st2(st1);	
	EXPECT_NE(&st1, &st2);
}