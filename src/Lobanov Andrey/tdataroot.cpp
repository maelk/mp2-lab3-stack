#include "tdataroot.h"

TDataRoot::TDataRoot(int Size) : TDataCom()
{
	if (Size < 0)
		throw SetRetCode(DataNoMem);
	else
	{
		MemSize = Size;
		DataCount = 0;
		if (Size == 0)
		{
			pMem = nullptr;
			MemType = MEM_RENTER;
		}
		else
		{
			pMem = new TElem[MemSize];
			MemType = MEM_HOLDER;
		}
	}
}

TDataRoot::~TDataRoot()
{
	delete[] pMem;	
}

void TDataRoot::SetMem(void *p, int Size)
{
	if (Size < 0)
		throw SetRetCode(DataNoMem);
	for (int i = 0; i < DataCount; i++)
		((PTElem)p)[i] = pMem[i];
	if (MemType == MEM_HOLDER)
	{
		delete[]pMem;
		pMem = new TElem[Size];
	    for (int i = 0; i < DataCount; i++)
			pMem[i] = ((PTElem)p)[i];
		MemType = MEM_RENTER;
	}
	pMem = (PTElem)p;
	MemSize = Size;
}

bool TDataRoot::IsEmpty(void) const
{
	return DataCount == 0;
}

bool TDataRoot::IsFull(void) const
{
	return DataCount == MemSize;
}
