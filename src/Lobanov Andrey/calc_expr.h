#ifndef __CALCEXPR_H__
#define __CALCEXPR_H__

#include <string>
#include <iostream>
#include "TStack.h"
using namespace std;

bool Expression_Check(const string& IncomStr);  //�������� �� ������������ �������
bool Brackets_Check(const string& IncomStr);    // �������� ����������� ������
int Oper_Prior(char);                      // ������������ ���������� ���������
bool Operator_Check(char);                 // �������� ���������
string PostfixForm(const string& IncomStr);      // ������� � ����������� �����
TData Exp_result(const string& PostfixStr);    // ���������� �� ����������� �����
TData Calculate(const string& IncomStr);       // �������� ����������

#endif
