#include "TStack.h"
#include <iostream>
#include "calc_expr.h"
using namespace std;

int main()
{
	try
	{	
		string str = "(1+2)/(3+4*6.7)-5.3*4.4";
		cout << "Your arithmetic string:" << str << endl;
		cout << "Table brackets:" << endl;
		cout << "Answer = " << Calculate(str) << endl;
	}
	catch (int err)
	{
		cout << err << endl;
	}
	return 0;
}