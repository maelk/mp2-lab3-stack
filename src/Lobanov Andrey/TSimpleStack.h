#ifndef __SIMPLESTACK_H__
#define __SIMPLESTACK_H__

#define DefMemSize 25

class TSimpleStack
{
private:
	int top;
	int tmp[DefMemSize];
public:
	TSimpleStack() { top = -1; }
	TSimpleStack(const TSimpleStack &newst)
	{
		top = newst.top;
		for (int i = 0; i < top; i++)
			tmp[i] = newst.tmp[i];
	}
	bool IsFull() const { return top == DefMemSize - 1; }
	bool IsEmpty() const { return top == -1; }
	void Push(const int num)
	{
		if (IsFull())
			throw "Stack is full";
		tmp[++top] = num;
	}
	int Pop()
	{
		if (IsEmpty())
			throw "Stack is empty";
		return tmp[top--];
	}
};

#endif