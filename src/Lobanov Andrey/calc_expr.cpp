#include "calc_expr.h"

bool Expression_Check(const string& inputstr)
{
	for (string::const_iterator it = inputstr.begin(); it != inputstr.end(); ++it)
		if (*it < '(' || *it > '9')
			return false;
	return true;
}

bool Brackets_Check(const string& inputstr)
{
	TStack Brackets;
	int counter = 0, Index = 0;
	bool result = true;
	if (Expression_Check(inputstr))
	{
	for (string::const_iterator it = inputstr.begin(); it != inputstr.end(); ++it)
	{
		if (*it == '(')
		{
			Index++;
			Brackets.Put((TData)Index);
		}
		if (*it == ')')
		{
			Index++;
			if (Brackets.IsEmpty())
			{
				counter++;
				cout << "-  " << Index << endl;
			}
			else
				cout << Brackets.Get() << "  " << Index << endl;
		}
	}
	while (!Brackets.IsEmpty())
	{
		cout << Brackets.Get() << "  -" << endl;
		counter++;
	}
	if (counter > 0)
		result = false;
	return result;
	}
	else 
		throw "Incorrect experssion";
}

int Oper_Prior(char st)
{
	switch (st)
	{
	case '(': return 0;
	case ')': return 1;
	case '+': return 2;
	case '-': return 2;
	case '*': return 3;
	case '/': return 3;
	default: return -1;
	}
}

bool Operator_Check(char inpustsymb)
{
	if (inpustsymb == '(' || inpustsymb == ')' || inpustsymb == '+' || inpustsymb == '-' || inpustsymb == '*' || inpustsymb == '/')
		return true;
	else
		return false;
}

string PostfixForm(const string& inputstr)
{
	string PostfixStr;
	TStack st;
	if (!Brackets_Check(inputstr))
		throw "Incorrect arithmetic expression";
	for (string::const_iterator it = inputstr.begin(); it != inputstr.end(); ++it)
	{
		if (!Operator_Check(*it))
			PostfixStr.push_back(*it);
		else
		{
			PostfixStr.push_back(' ');
			if (Oper_Prior(*it) == 0 || st.IsEmpty() || Oper_Prior(*it) >  Oper_Prior(st.GetElem()))
				st.Put(*it);
			else if (*it == ')')
			{
				while (st.GetElem() != '(')
					PostfixStr.push_back((char)st.Get());
				st.Get();
			}
			else
			{
				while (!st.IsEmpty() &&  Oper_Prior(*it) <=  Oper_Prior(st.GetElem()))
					PostfixStr.push_back((char)st.Get());
				st.Put(*it);
			}
		}
	}
	PostfixStr.push_back(' ');
	while (!st.IsEmpty())
		PostfixStr.push_back((char)st.Get());
	return PostfixStr;
}

TData Exp_result(const string& PostfixStr)
{
	TStack Ch;
	string str;
	TData item1 = 0, item2 = 0, result;
	for (string::const_iterator it = PostfixStr.begin(); it != PostfixStr.end(); ++it)
	{
		if (Operator_Check(*it))
		{
			item2 = Ch.Get();
			item1 = Ch.Get();
			if(*it == '+')			
				result = item1 + item2;
			else if(*it == '-')
				result = item1 - item2;
			else if (*it == '*')
				result = item1 * item2;
			else if (*it == '/')
				result = item1 / item2;		

			Ch.Put(result);
		}
		else if (*it != ' ')
			str.push_back(*it);
		else if (*it == ' ' && str != "")
		{
			Ch.Put(atof(str.c_str()));  //������� atof ����������� ������ � �������� ���� double, ����������� �������, �� ��� ���, ���� �� ����� ������ ������ ������, �������� �� �������.
			str = "";
		}
	}
	return Ch.Get();
}

TData Calculate(const string& inputstr)
{
	return Exp_result(PostfixForm(inputstr));
}

