﻿# Методы программирования 2: Стек


## Цели и задачи


 __Цели данной работы__ В рамках лабораторной работы ставится цель разработки двух видов стеков:

* прстейшего, основанного на статическом массиве (класс TSimpleStack);
* более сложного, основанного на использовании динамической структуры (класс TStack).

С помощью разработанных стеков необходимо написать приложение, которое вычисляет арифметическое выражение, заданное в виде строки и вводится пользователем. 

В процессе выполнения лабораторной работы требуется использовать систему контроля версий Git и фрэймворк для разработки автоматических тестов Google Test.

Перед выполнением работы студенты получают данный проект-шаблон, содержащий следующее:

 - Интерфейсы классов `TDataCom` и `TDataRoot` (h-файлы)
 - Тестовый пример использования класса `TStack`
 
 __Задачи:__
 
1. Разработка класса TSimpleStack на основе массива фиксированной длины.
2. Реализация методов класса TDataRoot согласно заданному интерфейсу.
3. Разработка класса TStack, являющегося производным классом от TDataRoot.
4. Разработка тестов для проверки работоспособности стеков.
5. Реализация алгоритма проверки правильности введенного арифметического выражения.
6. Реализация алгоритмов разбора и вычисления арифметического выражения.
7. Обеспечение работоспособности тестов и примера использования.

## Начало работы:

Абстрактный базовый класс `TDataCom` используется в качестве обработчика ошибок для наследуемых классов `TDataRoot` и `TStack`.

```c++
#ifndef __DATACOM_H__
#define __DATACOM_H__

#define DataOK   0
#define DataErr -1

// TDataCom является общим базовым классом
class TDataCom
{
protected:
  int RetCode; // Код завершения

  int SetRetCode(int ret) { return RetCode = ret; }
public:
  TDataCom(): RetCode(DataOK) {}
  virtual ~TDataCom() = 0 {}
  int GetRetCode()
  {
    int temp = RetCode;
    RetCode = DataOK;
    return temp;
  }
};

#endif
```

### 1. Разработка класса TSimpleStack на основе массива фиксированной длины:


```c++
#ifndef __SIMPLESTACK_H__
#define __SIMPLESTACK_H__

#define DefMemSize 25

class TSimpleStack
{
private:
	int top;
	int tmp[DefMemSize];
public:
	TSimpleStack() { top = -1; }
	TSimpleStack(const TSimpleStack &newst)
	{
		top = newst.top;
		for (int i = 0; i < top; i++)
			tmp[i] = newst.tmp[i];
	}
	bool IsFull() const { return top == DefMemSize - 1; }
	bool IsEmpty() const { return top == -1; }
	void Push(const int num)
	{
		if (IsFull())
			throw "Stack is full";
		tmp[++top] = num;
	}
	int Pop()
	{
		if (IsEmpty())
			throw "Stack is empty";
		return tmp[top--];
	}
};

#endif
```

### 2. Заданный интерфейс класса TDataRoot:

#### Объявление:

```c++
// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tdataroot.h - Copyright (c) Гергель В.П. 28.07.2000 (06.08)
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (21.04.2015)
//
// Динамические структуры данных - базовый (абстрактный) класс - версия 3.2
//   память выделяется динамически или задается методом SetMem

#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include "tdatacom.h"

#define DefMemSize   25  // размер памяти по умолчанию

#define DataEmpty  -101  // СД пуста
#define DataFull   -102  // СД переполнена
#define DataNoMem  -103  // нет памяти

typedef double  TElem;    // тип элемента СД
typedef TElem* PTElem;
typedef double  TData;    // тип значений в СД

enum TMemType { MEM_HOLDER, MEM_RENTER };

class TDataRoot: public TDataCom
{
protected:
  PTElem pMem;      // память для СД
  int MemSize;      // размер памяти для СД
  int DataCount;    // количество элементов в СД
  TMemType MemType; // режим управления памятью

  void SetMem(void *p, int Size);             // задание памяти
public:
  virtual ~TDataRoot();
  TDataRoot(int Size = DefMemSize);
  virtual bool IsEmpty(void) const;           // контроль пустоты СД
  virtual bool IsFull (void) const;           // контроль переполнения СД
  virtual void  Put   (const TData &Val) = 0; // добавить значение
  virtual TData Get   (void)             = 0; // извлечь значение

  // служебные методы
  virtual int  IsValid() = 0;                 // тестирование структуры
  virtual void Print()   = 0;                 // печать значений

  // дружественные классы
  friend class TMultiStack;
  friend class TSuperMultiStack;
  friend class TComplexMultiStack;
};

#endif
```

#### Реализация класса TDataRoot:

```c++
#include "tdataroot.h"

TDataRoot::TDataRoot(int Size) : TDataCom()
{
	if (Size < 0)
		throw SetRetCode(DataNoMem);
	else
	{
		MemSize = Size;
		DataCount = 0;
		if (Size == 0)
		{
			pMem = nullptr;
			MemType = MEM_RENTER;
		}
		else
		{
			pMem = new TElem[MemSize];
			MemType = MEM_HOLDER;
		}
	}
}

TDataRoot::~TDataRoot()
{
	delete[] pMem;	
}

void TDataRoot::SetMem(void *p, int Size)
{
	if (Size < 0)
		throw SetRetCode(DataNoMem);
	for (int i = 0; i < DataCount; i++)
			((PTElem)p)[i] = pMem[i];
	if (MemType == MEM_HOLDER)
	{
		delete[]pMem;
		pMem = new TElem[Size];
	    for (int i = 0; i < DataCount; i++)
		pMem[i] = ((PTElem)p)[i];
		MemType = MEM_RENTER;
	}
	pMem = (PTElem)p;
	MemSize = Size;
}

bool TDataRoot::IsEmpty(void) const
{
	return DataCount == 0;
}

bool TDataRoot::IsFull(void) const
{
	return DataCount == MemSize;
}
```

### 3. Реализация класса TStack:

#### Объявление:

```c++
#ifndef __TSTACK_H__
#define __TSTACK_H__

#include "tdataroot.h"

class TStack : public TDataRoot
{
private:
	int Top;
public:
	TStack(int Size = DefMemSize) : TDataRoot(Size) { Top = -1; }
	TStack(const TStack&);
	void  Put(const TData&);
	TData Get(void);
	int Get_Size() { return MemSize; }          //получение размера
	TData GetElem() const { return pMem[Top]; } //взятие текущего элемента
	void Print();
	int  IsValid();
};

#endif

```

#### Реализация:

```c++
#include "TStack.h"
#include <iostream>
using namespace std;

TStack::TStack(const TStack& NewStack)
{
	MemSize = NewStack.MemSize;
	DataCount = NewStack.DataCount;
	MemType = NewStack.MemType;
	pMem = new TElem[MemSize];
	for (int i = 0; i < MemSize; ++i)
	{
		pMem[i] = NewStack.pMem[i];
	}
}

void TStack::Put(const TData& tmp)
{
	if (pMem == nullptr) 
	{ 
		throw SetRetCode(DataNoMem); 
	}
	else if (IsFull())
	{
		void* p = nullptr;
		SetMem(p, MemSize + DefMemSize);
		pMem[++Top] = tmp;
		DataCount++;
	}
	else
	{
		pMem[++Top] = tmp;
		DataCount++;
	}
}

TData TStack::Get(void)
{
	if (pMem == nullptr)	
		throw SetRetCode(DataNoMem);	
	else if (IsEmpty())	
		throw SetRetCode(DataEmpty);	
	else
	{
		DataCount--;
		return pMem[Top--];
	}
}

void TStack::Print()
{
	if (DataCount == 0) { cout << "Stack is empty!"; }
	for (int i = 0; i < DataCount; ++i)
	{
		cout << pMem[i] << " ";
	}
	cout << endl;
}

int  TStack::IsValid()
{
	int res = 0;
	if (pMem == nullptr)
		res++;
	if (MemSize < DataCount)
		res += 2;
	return res;
}

```
####Тестовый пример использования класса TStack:

#include "TStack.h"
#include <iostream>

using namespace std;

void main()
{
  TStack st(35);
  int temp;

  
  cout << "Testing stack" << endl;
  for (int i = 0; i < 35; i++)
  {
    st.Put(i);
    cout << "Put Value " << i << " Code " << st.GetRetCode() << endl;
  }
  while (!st.IsEmpty())
  {
    temp = st.Get();
    cout << "Get Value " << temp << " Kod " << st.GetRetCode() << endl;
  }
}

**Результат работы программы:
https://pp.vk.me/c639125/v639125927/9ff1/3G-cd_nu4dE.jpg

#### 4. Тесты:

#### Тесты для класса TSimpleStack:

```c++

#include "TSimpleStack.h"

#include "gtest.h"

TEST(TSimpleStack, created_stack_is_empty)
{
	TSimpleStack st;
	EXPECT_TRUE(st.IsEmpty());
}

TEST(TSimpleStack, can_pop_from_stack)
{
	TSimpleStact st;
	st.Push(0);
	EXPECT_EQ(0, st.Pop());
}
TEST(TSimpleStack, can_pop_from_full_stack)
{
	TSimpleStack st;
	for(int i = 0; i < DefMemSize; ++i)
		st.Push(0);
	st.Pop();
	EXPECT_FALSE(st.IsFull());
}

TEST(TSimpleStack, can_push_in_stack)
{
	TSimpleStack st;
	st.Push(5);
	EXPECT_FALSE(st.IsEmpty());
}

TEST(TSimpleStack, cant_pop_from_empty_stack)
{
	TSimpleStack st;	
	EXPECT_ANY_THROW(st.Pop());
}

TEST(TSimpleStack, cant_push_in_full_stack)
{
	TSimpleStack st;
	for (int i = 0; i < DefMemSize; ++i)
		st.Push(0);
	EXPECT_ANY_THROW(st.Push(0));
}

TEST(TSimpleStack, can_copy_stack)
{
	TSimpleStack st1;
	for (int i = 0; i < DefMemSize; ++i)
		st1.Push(0);
	EXPECT_NO_THROW(TSimpleStack st2(st1));
}


TEST(TSimpleStack, copied_stack_has_its_own_memory)
{
	TSimpleStack st1;
	for (int i = 0; i < DefMemSize; ++i)
		st1.Push(0);
	TSimpleStack st2(st1);	
	EXPECT_NE(&st1, &st2);
}
```

#### Тесты для класса TStack:

```c++

#include "TStack.h"
#include "gtest.h"

TEST(TStack, created_stack_is_empty)
{
	TStack st;
	EXPECT_TRUE(st.IsEmpty());
}

TEST(TStack, can_put_in_stack)
{
	TStack st;
	st.Put(0);
	EXPECT_FALSE(st.IsEmpty());
}

TEST(TStack, can_get_from_stack)
{
	TStack st;
	st.Put(0);
	EXPECT_EQ(0, st.Get());
}


TEST(TStack, size_is_displayed_correctly)
{
	TStack st(45);
	EXPECT_EQ(45, st.Get_Size());
}

TEST(TStack, current_element_is_displayed_correctly)
{
	TStack st;
	st.Put(0);
	EXPECT_EQ(0, st.GetElem());
}

TEST(TStack, cant_get_from_empty_stack)
{
	TStack st;	
	EXPECT_ANY_THROW(st.Get());
}

TEST(TStack, can_get_from_full_stack)
{
	TStack st;
	for (int i = 0; i < st.Get_Size(); ++i)
		st.Put(0);
	st.Get();
	EXPECT_FALSE(st.IsFull());
}

TEST(TStack, can_put_in_full_stack)
{
	TStack st;	
	for (int i = 0; i < st.Get_Size(); ++i)
		st.Put(0);
	st.Put(0);
	EXPECT_NE(DefMemSize, st.Get_Size());
}

TEST(TStack, can_copy_stack)
{
	TStack st1;
	for (int i = 0; i < st1.Get_Size(); ++i)
		st1.Put(0);
	EXPECT_NO_THROW(TStack st2(st1));
}


TEST(TStack, copied_stack_has_its_own_memory)
{
	TStack st1;
	for (int i = 0; i < st1.Get_Size(); ++i)
		st1.Put(0);
	TStack st2(st1);
	EXPECT_NE(&st1, &st2);
}


```
**Результат работы тестов:

https://pp.vk.me/c639125/v639125927/a00a/QcL5roqWrCM.jpg

#### 5-6. Реализация алгоритма проверки правильности введенного арифметического выражения и алгоритмов разбора и вычисления арифметического выражения.

**Реализация алгоритма проверки правильности введенного арифметического выражения**
Для проверки правильности введенного арифметического выражения в рамках данной работы используется проверка скобок

На вход алгоритма поступает строка символов, на выходе должна быть выдана таблица соответствия номеров открывающихся и закрывающихся скобок и общее количество ошибок. Идея алгоритма, решающего поставленную задачу, состоит в следующем.
- Выражение просматривается посимвольно слева направо. Все символы, кроме скобок, игнорируются (т.е. просто производится переход к просмотру следующего символа).
- Если очередной символ – открывающая скобка, то её порядковый номер помещается в стек.
- Если очередной символ – закрывающая скобка, то производится выталкивание из стека номера открывающей скобки и запись этого номера в паре с номером закрывающей скобки в результирующую таблицу.
- Если в этой ситуации стек оказывается пустым, то вместо номера открывающей скобки записывается 0, а счетчик ошибок увеличивается на единицу.
- Если после просмотра всего выражения стек оказывается не пустым, то выталкиваются все оставшиеся номера открывающих скобок и записываются в результирующий массив в паре с 0 на месте номера закрывающей скобки, счетчик ошибок каждый раз увеличивается на единицу.


**Понятие постфиксной формы записи выражения**

В рамках данного задания был разработан алгоритм и составлена программа для перевода арифметического выражения из инфиксной формы записи в постфиксную. **Инфиксная форма** записи характеризуется наличием знаков операций между операндами. Например,

```
(1+2)/(3+4*6.7)-5.3*4.4
```

При такой форме записи порядок действий определяется расстановкой скобок и приоритетом операций. **Постфиксная форма записи** не содержит скобок, а знаки операций следуют после соответствующих операндов. Тогда для приведённого примера постфиксная форма будет иметь вид:

```
1 2+ 3 4 6.7*+/ 5.3 4.4* -
```
**Реализация алгоритмов разбора и вычисления арифметического выражения:**

Данный алгоритм основан на использовании стека. На вход алгоритма поступает строка символов, на выходе должна быть получена строка с постфиксной формой. Каждой операции и скобкам приписывается приоритет.

- ( - 0

- ) - 1

- +- - 2

- */ - 3

Предполагается, что входная строка содержит синтаксически правильное выражение.
Входная строка просматривается посимвольно слева направо до достижения конца строки. Операндами будем считать любую последовательность символов входной строки, не совпадающую со знаками определённых в таблице операций. Операнды по мере их появления переписываются в выходную строку. При появлении во входной строке операции, происходит вычисление приоритета данной операции. Знак данной операции помещается в стек, если:
- Приоритет операции равен 0 (это « ( » ),
- Приоритет операции строго больше приоритета операции, лежащей на вершине стека,
- Стек пуст.

В противном случае из стека извлекаются все знаки операций с приоритетом больше или равным приоритету текущей операции. Они переписываются в выходную строку, после чего знак текущей операции помещается в стек. Имеется особенность в обработке закрывающей скобки. Появление закрывающей скобки во входной строке приводит к выталкиванию и записи в выходную строку всех знаков операций до появления открывающей скобки. Открывающая скобка из стека выталкивается, но в выходную строку не записывается. Таким образом, ни открывающая, ни закрывающая скобки в выходную строку не попадают. После просмотра всей входной строки происходит последовательное извлечение всех элементов стека с одновременной записью знаков операций, извлекаемых из стека, в выходную строку.

Алгоритм вычисления арифметического выражения за один просмотр входной строки основан на использовании постфиксной формы записи выражения и работы со стеком.
Выражение просматривается посимвольно слева направо. При обнаружении операнда производится перевод его в числовую форму и помещение в стек (если операнд не является числом, то вычисление прекращается с выдачей сообщения об ошибке.) При обнаружении знака операции происходит извлечение из стека двух значений, которые рассматриваются как операнд2 и операнд1 соответственно, и над ними производится обрабатываемая операция. Результат этой операции помещается в стек. По окончании просмотра всего выражения из стека извлекается окончательный результат.

#### Объявление функций:

```c++

#ifndef __CALCEXPR_H__
#define __CALCEXPR_H__

#include <string>
#include <iostream>
#include "TStack.h"
using namespace std;

bool Expression_Check(const string& IncomStr);  //Проверка на недопустимые символы
bool Brackets_Check(const string& IncomStr);    // Проверка расстановки скобок
int Oper_Prior(char);                      // Присваивание приоритета операциям
bool Operator_Check(char);                 // Проверка оператора
string PostfixForm(const string& IncomStr);      // Перевод в постфиксную форму
TData Exp_result(const string& PostfixStr);    // Вычисление по постфиксной форме
TData Calculate(const string& IncomStr);       // Конечное вычисление

#endif
```

#### Реализация:

```c++

#include "calc_expr.h"

bool Expression_Check(const string& inputstr)
{
	for (string::const_iterator it = inputstr.begin(); it != inputstr.end(); ++it)
		if (*it < '(' || *it > '9')
			return false;
	return true;
}

bool Brackets_Check(const string& inputstr)
{
	TStack Brackets;
	int counter = 0, Index = 0;
	bool result = true;
	if (Expression_Check(inputstr))
	{
	for (string::const_iterator it = inputstr.begin(); it != inputstr.end(); ++it)
	{
		if (*it == '(')
		{
			Index++;
			Brackets.Put((TData)Index);
		}
		if (*it == ')')
		{
			Index++;
			if (Brackets.IsEmpty())
			{
				counter++;
				cout << "  " << Index << endl;
			}
			else
				cout << Brackets.Get() << "  " << Index << endl;
		}
	}
	while (!Brackets.IsEmpty())
	{
		cout << Brackets.Get() << "  " << endl;
		counter++;
	}
	if (counter > 0)
		result = false;
	return result;
	}
	else 
		throw "Incorrect experssion";
}

int Oper_Prior(char st)
{
	switch (st)
	{
	case '(': return 0;
	case ')': return 1;
	case '+': return 2;
	case '-': return 2;
	case '*': return 3;
	case '/': return 3;
	default: return -1;
	}
}

bool Operator_Check(char inpustsymb)
{
	if (inpustsymb == '(' || inpustsymb == ')' || inpustsymb == '+' || inpustsymb == '-' || inpustsymb == '*' || inpustsymb == '/')
		return true;
	else
		return false;
}

string PostfixForm(const string& inputstr)
{
	string PostfixStr;
	TStack st;
	if (!Brackets_Check(inputstr))
		throw "Incorrect arithmetic expression";
	for (string::const_iterator it = inputstr.begin(); it != inputstr.end(); ++it)
	{
		if (!Operator_Check(*it))
			PostfixStr.push_back(*it);
		else
		{
			PostfixStr.push_back(' ');
			if (Oper_Prior(*it) == 0 || st.IsEmpty() || Oper_Prior(*it) >  Oper_Prior(st.GetElem()))
				st.Put(*it);
			else if (*it == ')')
			{
				while (st.GetElem() != '(')
					PostfixStr.push_back((char)st.Get());
				st.Get();
			}
			else
			{
				while (!st.IsEmpty() &&  Oper_Prior(*it) <=  Oper_Prior(st.GetElem()))
					PostfixStr.push_back((char)st.Get());
				st.Put(*it);
			}
		}
	}
	PostfixStr.push_back(' ');
	while (!st.IsEmpty())
		PostfixStr.push_back((char)st.Get());
	return PostfixStr;
}

TData Exp_result(const string& PostfixStr)
{
	TStack Ch;
	string str;
	TData item1 = 0, item2 = 0, result;
	for (string::const_iterator it = PostfixStr.begin(); it != PostfixStr.end(); ++it)
	{
		if (Operator_Check(*it))
		{
			item2 = Ch.Get();
			item1 = Ch.Get();
			if(*it == '+')			
				result = item1 + item2;
			else if(*it == '-')
				result = item1 - item2;
			else if (*it == '*')
				result = item1 * item2;
			else if (*it == '/')
				result = item1 / item2;		

			Ch.Put(result);
		}
		else if (*it != ' ')
			str.push_back(*it);
		else if (*it == ' ' && str != "")
		{
			Ch.Put(atof(str.c_str()));  //функция atof преобразует строку в значение типа double, отбрасывает пробелы, до тех пор, пока не будет найден первый символ, отличный от пробела.
			str = "";
		}
	}
	return Ch.Get();
}

TData Calculate(const string& inputstr)
{
	return Exp_result(PostfixForm(inputstr));
}
```

#### 7. Работоспособность тестов и примера использования:

#### Тесты для функций вычисления арифметического выражения calc_expr:

```c++

#include "TStack.h"
#include "calc_expr.h"
#include <string>

#include "gtest.h"

TEST(calc_expr, can_check_expression)
{
	string str = "(1+2)";
	EXPECT_TRUE(Expression_Check(str));
}

TEST(calc_expr, expression_checked_correctly)
{
	string str = "(1+2q)";
	EXPECT_FALSE(Expression_Check(str));
}

TEST(calc_expr, can_check_brackets)
{
	string str = "(1+2)";
	EXPECT_TRUE(Brackets_Check(str));
}

TEST(calc_expr, brackets_checked_correctly)
{
	string str = "(1+2";
	EXPECT_FALSE(Brackets_Check(str));
}

TEST(calc_expr, conversion_to_postfix_form_is_correctly)
{
	string str = "(1+2)/(3*4)-5";
	EXPECT_EQ("1 2 +3 4 */5 -", PostfixForm(str));
}

TEST(calc_expr, calculating_is_correctly)
{
	string str = "(1+2)/(3*4)-5";
	EXPECT_EQ(-4.75, Calculate(str));
}
```
**Результат работы тестов:

https://pp.vk.me/c639125/v639125927/a00a/QcL5roqWrCM.jpg

#### Пример использования:

```c++
#include "TStack.h"
#include <iostream>
#include "calc_expr.h"
using namespace std;

int main()
{
	try
	{	
		string str = "(1+2)/(3+4*6.7)-5.3*4.4";
		cout << "Your arithmetic string:" << str << endl;
		cout << "Table brackets:" << endl;
		cout << "Answer = " << Calculate(str) << endl;
	}
	catch (int err)
	{
		cout << err << endl;
	}
	return 0;
}
```
**Результат работы программы:
https://pp.vk.me/c639125/v639125927/9ffa/Tj_l39eYCpQ.jpg

#### Вывод:

В ходе данной работы были самостоятельно реализованы два класса:
* TSimpleStack простая версия стека на основе статического массива.
* TStack более сложная версия стека, использующая динамическую память, позволяющую стеку увеличивать размер памяти, если её не достаточно для новых элементов.

Так же были реализованы функции позволяющие переводить арифметические выражения,заданные в виде строки символов, в постфиксную форму записи и вычислять их, причём за один проход по строке.

Были разработаны и пройдены тесты, которые позволили найти и устранить ошибки, незаметные на первый взгляд.