#ifndef __TSTACK_H__
#define __TSTACK_H__

#include "tdataroot.h"

class TStack : public TDataRoot
{
private:
	int Top;
public:
	TStack(int Size = DefMemSize) : TDataRoot(Size) { Top = -1; }
	TStack(const TStack&);
	void  Put(const TData&);
	TData Get(void);
	int Get_Size() { return MemSize; }          //��������� �������
	TData GetElem() const { return pMem[Top]; } //������ �������� ��������
	void Print();
	int  IsValid();
};

#endif

