#include "TStack.h"
#include <string>
#include <iostream>

#define NumOfPriority 4

using namespace std;

class arithmetic
{
protected:
	string postfix, infix;                                   // ������ ��� �������� ����������� � ��������� ����� ������
	double result;											// ������ ��� �������� ����������
	string priority[NumOfPriority] = { "(",")","+-","*/" }; // �������� � �� ���������
	string InfToPost(string str);							// ����� �������� �� ��������� � ����������� ������
	double PostToResult(string str);						// ����� ������� ���������� �� ����������� �����
	double StrToDouble(string str);							// ������� �� string � double
	bool IsValid(string str);								// �������� ������������ ����� ������
	int PriorityOfChar(char ch);
	double Operations(double num1, double num2, char operation);
public:
	arithmetic(string str = "");
	void PutInfix(string str);
	string GetPostfix() { return postfix; };
	string GetInfix() { return infix; };
	double GetResult() { return result; };
	void PrintInfix() { cout << infix << endl; };
	void PrintPostfix() { cout << postfix << endl; };
	void PrintResult() { cout << result << endl; };
	friend ostream& operator<<(ostream &out, const arithmetic &a)
	{
		out << "postfix: " << a.postfix << endl;
		out << "infix: " << a.infix << endl;
		out << "result: " << a.result << endl;
		return out;
	}
};