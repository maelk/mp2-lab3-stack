# Методы программирования 2: Стек

## 1. Введение

Лабораторная работа направлена на практическое освоение динамической структуры данных Стек. В качестве области приложений выбрана тема вычисления арифметических выражений, возникающей при трансляции программ на языке программирования высокого уровня в исполняемые программы.
При вычислении произвольных арифметических выражений возникают две основные задачи: проверка корректности введённого выражения и выполнение операций в порядке, определяемом их приоритетами и расстановкой скобок. Существует алгоритм, позволяющий реализовать вычисление произвольного арифметического выражения за один просмотр без хранения промежуточных результатов. Для реализации данного алгоритма выражение должно быть представлено в постфиксной форме. Рассматриваемые в данной лабораторной работе алгоритмы являются начальным введением в область машинных вычислений.

## 2. Цели и задачи

В рамках лабораторной работы ставится задача разработки двух видов стеков:

- простейшего, основанного на статическом массиве (класс `TSimpleStack`);
- более сложного, основанного на использовании динамической структуры (класс `TStack`).

С помощью разработанных стеков необходимо написать приложение, которое вычисляет арифметическое выражение, заданное в виде строки и вводится пользователем. Сложность выражения ограничена только длиной строки.

В процессе выполнения лабораторной работы требуется использовать систему контроля версий Git и фрэймворк для разработки автоматических тестов Google Test.

Перед выполнением работы студенты получают данный проект-шаблон, содержащий следующее:

 - Интерфейсы классов `TDataCom` и `TDataRoot` (h-файлы)
 - Тестовый пример использования класса `TStack`

Выполнение работы предполагает решение следующих задач:

  1. Разработка класса `TSimpleStack` на основе массива фиксированной длины.
  1. Реализация методов класса `TDataRoot` согласно заданному интерфейсу.
  1. Разработка класса `TStack`, являющегося производным классом от `TDataRoot`.
  1. Разработка тестов для проверки работоспособности стеков.
  2. Реализация алгоритма проверки правильности введенного арифметического выражения.
  2. Реализация алгоритмов разбора и вычисления арифметического выражения.
  1. Обеспечение работоспособности тестов и примера использования.

## 3. Выполнение работы

### 3.1 Разработка класса TSimpleStack

Простой стек `TSimpleStack` основан на том, что участок для хранения памяти выделяется один раз при создании объекта и остается неизменным вплоть до удаления объекта. При попытке вставить элемент в заполненный стек программа выдаст исключение.

Виды выбрасываемых исключений следующие:


1) 1 - Введен некорректный размер

2) 2 - Попытка вставить элемент в заполненный стек

3) 3 - Попытка взять элемент из пустого стека
 
Класс `TSimpleStack` построен на шаблонах, что позволяет использовать его для разных типов данных.

*Код tsimplestack.h*

	#ifndef _TSIMPLESTACK_
	#define _TSIMPLESTACK_
	
	#include <iostream>
	
	template <class T>
	class TSimpleStack {
	private:
		int top, size;
		T* data;
	
	public:
		TSimpleStack(int = 20);
		TSimpleStack(const TSimpleStack&);
		~TSimpleStack();
		void put(const T&); //положить элемент в стек
		T get(); //вытащить элемент из стека
		void print() const; //печать стека на экран
	};
	
	template <class T>
	TSimpleStack<T>::TSimpleStack(int s)
	{
		if (s > 0)
		{
			size = s;
			data = new T[size];
			top = -1;
		}
		else
			throw 1;
	}
	
	template <class T>
	TSimpleStack<T>::TSimpleStack(const TSimpleStack &st)
	{
		size = st.size;
		top = st.top;
	
		data = new T[size];
	
		for (int i = 0; i <= top; i++)
			data[i] = st.data[i];
	}
	
	template <class T>
	TSimpleStack<T>::~TSimpleStack()
	{
		delete[] data;
	}
	
	template <class T>
	void TSimpleStack<T>::put(const T& value)
	{
		if (top < size - 1)
			data[++top] = value;
		else
			throw 2;
	}
	
	template <class T>
	T TSimpleStack<T>::get()
	{
		if (top > -1)
			return data[top--];
		else
			throw 3;
	}
	
	template <class T>
	void TSimpleStack<T>::print() const
	{
		for (int i = top; i > -1; i--)
			std::cout << data[i] << std::endl;
	}
	#endif

### 3.2 Реализация методов класса TDataRoot

Несмотря на то, что в работе уже был дан готовый интерфейс *tdataroot.h*, он был переработан, а именно переделан под шаблоны, для того, чтобы в дальнейшем класс `TStack` мог использоваться для вычисления выражений, ведь при преобразовании выражения из инфиксной формы в постфиксную нам понадобится стек для хранения символов (то есть тип `char`), а при непосредственном вычислении выражения, в стеке необходимо будет хранить вещественные значения. Также в класс `TDataRoot` был добавлен конструктор копирования.

*Код tdataroot.h *

	#ifndef __DATAROOT_H__
	#define __DATAROOT_H__
	
	#include "tdatacom.h"
	
	#define DefMemSize   25  // размер памяти по умолчанию
	
	#define DataEmpty  -101  // СД пуста
	#define DataFull   -102  // СД переполнена
	#define DataNoMem  -103  // нет памяти
	#define SizeIncorrect -104 // размер СД некорректен
	
	enum TMemType { MEM_HOLDER, MEM_RENTER };
	
	template <class T>
	class TDataRoot : public TDataCom
	{
	protected:
		T* pMem;      // память для СД
		int MemSize;      // размер памяти для СД
		int DataCount;    // количество элементов в СД
		TMemType MemType; // режим управления памятью
	
		void SetMem(void *p, int Size);             // задание памяти
	public:
		virtual ~TDataRoot();
		TDataRoot(int Size = DefMemSize);
		TDataRoot(const TDataRoot&);
		virtual bool IsEmpty(void) const;           // контроль пустоты СД
		virtual bool IsFull(void) const;           // контроль переполнения СД
		virtual void  Put(const T &Val) = 0; // добавить значение
		virtual T Get(void) = 0; // извлечь значение
	
									 // служебные методы
		virtual void Print() = 0;                 // печать значений
	
												  // дружественные классы
		friend class TMultiStack;
		friend class TSuperMultiStack;
		friend class TComplexMultiStack;
	};
	
	template <class T>
	TDataRoot<T>::TDataRoot(int s) : TDataCom()
	{
		MemSize = s;
		DataCount = 0;
	
		if (s == 0)
		{
			pMem = nullptr;
			MemType = MEM_RENTER;
		}
		else if (s > 0)
		{
			pMem = new T[MemSize];
			MemType = MEM_HOLDER;
		}
		else
			throw SetRetCode(SizeIncorrect);
	}
	
	template <class T>
	TDataRoot<T>::TDataRoot(const TDataRoot &dr)
	{
		MemType = MEM_HOLDER;
		MemSize = dr.MemSize;
		DataCount = dr.DataCount;
	
		pMem = new T[MemSize];
	
		for (int i = 0; i < DataCount; i++)
			pMem[i] = dr.pMem[i];
	}
	
	template <class T>
	TDataRoot<T>::~TDataRoot()
	{
		if (MemType == MEM_HOLDER)
			delete[] pMem;
		pMem = nullptr;
	}
	
	template <class T>
	bool TDataRoot<T>::IsEmpty() const
	{
		return DataCount == 0;
	}
	
	template <class T>
	bool TDataRoot<T>::IsFull() const
	{
		return DataCount == MemSize;
	}
	
	template <class T>
	void TDataRoot<T>::SetMem(void *p, int Size)
	{
		if (MemType == MEM_HOLDER)
			delete pMem;
		pMem = (T*)p;
		MemSize = Size;
		MemType = MEM_RENTER;
	}
	
	#endif

### 3.3 Реализация класса TStack

Стек `TStack` основан на том, что в процессе работы программы выделенной при создании объекта памяти может не хватить, а значит ее нужно динамически увеличить. Увеличивать память будет метод `addMem()`. Количество памяти, на которое нужно увеличить имеющуюся, передается через конструктор, а по умолчанию равно 25.

*Код stack.h*

	#ifndef _TSTACK_
	#define _TSTACK_
	
	#include <iostream>
	#include "tdataroot.h"
	
	#define MemAddedIncorrect -105
	#define DefMemAdded 25
	
	template <class T>
	class TStack : public TDataRoot<T>
	{
	protected:
		int top, memAdded;
		void addMem();
	
	public:
		TStack(int Size = DefMemSize, int MemAdded = DefMemAdded);
		TStack(const TStack<T>&);
		void Put(const T&) override;
		T Get() override;
		void Print() override;
	};
	
	template <class T>
	void TStack<T>::addMem()
	{
		T* temp;
	
		temp = new T[MemSize + memAdded];
	
		for (int i = 0; i < DataCount; i++)
			temp[i] = pMem[i];
		delete[] pMem;
		pMem = temp;
	
		MemSize += memAdded;
	}
	
	template <class T>
	TStack<T>::TStack(int Size, int MemAdded) : TDataRoot<T>(Size)
	{
		top = -1;
	
		if (MemAdded > 0)
			memAdded = MemAdded;
		else
			throw SetRetCode(MemAddedIncorrect);
	}
	
	template <class T>
	TStack<T>::TStack(const TStack<T> &st) : TDataRoot<T>(st)
	{
		top = st.top;
		memAdded = st.memAdded;
	}
	
	template <class T>
	void TStack<T>::Put(const T &data)
	{
		if (pMem == nullptr)
			SetRetCode(DataNoMem);
		else if (IsFull())
		{
			if (MemType == MEM_RENTER)
				SetRetCode(DataFull);
			else
				addMem();
		}
		else
		{
			pMem[++top] = data;
			DataCount++;
		}
	}
	
	template <class T>
	T TStack<T>::Get()
	{
		if (pMem == nullptr)
			throw SetRetCode(DataNoMem);
		else if (IsEmpty())
			throw SetRetCode(DataEmpty);
		else
		{
			DataCount--;
			return pMem[top--];
		}
	}
	
	template <class T>
	void TStack<T>::Print()
	{
		for (int i = top; i > -1; i--)
			std::cout << pMem[i] << std::endl;
	}
	
	#endif

### 3.4 Тестирование классов TStack и TSimpleStack

Тестам даны такие имена, по которым без труда можно понять, что именно проверяет конкретный тест.

Для класса *TSimpleStack* были разработаны следующие тесты:

	TEST(TSimpleStack, cant_create_stack_with_negative_size)
	{
		ASSERT_ANY_THROW(TSimpleStack<int>(-1));
	}
	
	TEST(TSimpleStack, can_create_stack_with_positive_length)
	{
		ASSERT_NO_THROW(TSimpleStack<int>(10));
	}
	
	TEST(TSimpleStack, can_create_copied_stack)
	{
		TSimpleStack<int> st1(2);
	
		ASSERT_NO_THROW(TSimpleStack<int> st2 = st1);
	}
	
	TEST(TSimpleStack, copied_stack_is_equal_to_source_one)
	{
		TSimpleStack<int> st1(2);
	
		st1.put(1);
		st1.put(2);
	
		TSimpleStack<int> st2 = st1;
	
		EXPECT_EQ(1, (st2.get() == 2) && (st2.get() == 1));
	}
	
	TEST(TSimpleStack, copied_stack_has_its_own_memory)
	{
		TSimpleStack<int> st1(2);
	
		st1.put(1);
		st1.put(2);
	
		TSimpleStack<int> st2 = st1;
	
		st1.get();
	
		EXPECT_EQ(2, st2.get());
	}
	
	TEST(TSimpleStack, can_put_element_when_stack_isnt_full)
	{
		TSimpleStack<int> st1(2);
	
		ASSERT_NO_THROW(st1.put(1));
	}
	
	TEST(TSimpleStack, cant_put_element_when_stack_is_full)
	{
		TSimpleStack<int> st1(2);
	
		st1.put(1);
		st1.put(2);
	
		ASSERT_ANY_THROW(st1.put(3));
	}
	
	TEST(TSimpleStack, can_get_element_when_stack_isnt_empty)
	{
		TSimpleStack<int> st(2);
	
		st.put(2);
	
		ASSERT_NO_THROW(st.get());
	}
	
	TEST(TSimpleStack, cant_get_element_when_stack_is_empty)
	{
		TSimpleStack<int> st(2);
	
		ASSERT_ANY_THROW(st.get());
	}
	
	TEST(TSimpleStack, put_and_get_correctly)
	{
		TSimpleStack<int> st(1);
	
		st.put(1);
	
		EXPECT_EQ(1, st.get());
	}

Для класса *TStack* были разработаны следующие тесты:

	TEST(TStack, cant_create_stack_with_negative_size)
	{
		ASSERT_ANY_THROW(TStack<int>(-1));
	}
	
	TEST(TStack, null_size_stack_is_created_correctly)
	{
		ASSERT_NO_THROW(TStack<int>(0));
	}
	
	TEST(TStack, can_create_stack_with_positive_length)
	{
		ASSERT_NO_THROW(TStack<int>(10));
	}
	
	TEST(TStack, cant_create_stack_with_negative_mem_added)
	{
		ASSERT_ANY_THROW(TStack<int>(10, -1));
	}
	
	TEST(TStack, can_create_copied_stack)
	{
		TStack<int> st1(2);
	
		ASSERT_NO_THROW(TStack<int> st2 = st1);
	}
	
	TEST(TStack, copied_stack_is_equal_to_source_one)
	{
		TStack<int> st1(2);
	
		st1.Put(1);
		st1.Put(2);
	
		TStack<int> st2 = st1;
	
		EXPECT_EQ(1, (st2.Get() == 2) && (st2.Get() == 1));
	}
	
	TEST(TStack, copied_stack_has_its_own_memory)
	{
		TStack<int> st1(2);
	
		st1.Put(1);
		st1.Put(2);
	
		TStack<int> st2 = st1;
	
		st1.Get();
	
		EXPECT_EQ(2, st2.Get());
	}
	
	TEST(TStack, can_put_element_when_stack_isnt_full)
	{
		TStack<int> st1(2);
	
		ASSERT_NO_THROW(st1.Put(1));
	}
	
	TEST(TSimpleStack, can_put_element_when_stack_is_full)
	{
		TStack<int> st(2);
	
		st.Put(1);
		st.Put(2);
	
		ASSERT_NO_THROW(st.Put(3));
	}
	
	TEST(TSimpleStack, addMem_works_correctly)
	{
		TStack<int> st(2);
	
		st.Put(1);
		st.Put(2);
		st.Put(3);
	
		EXPECT_EQ(1, (st.Get() == 3) && (st.Get() == 2) && (st.Get() == 1));
	}
	
	TEST(TStack, can_get_element_when_stack_isnt_empty)
	{
		TStack<int> st(2);
	
		st.Put(2);
	
		ASSERT_NO_THROW(st.Get());
	}
	
	TEST(TStack, cant_get_element_when_stack_is_empty)
	{
		TStack<int> st(2);
	
		ASSERT_ANY_THROW(st.Get());
	}
	
	TEST(TStack, put_and_get_works_correctly)
	{
		TStack<int> st(1);
	
		st.Put(1);
	
		EXPECT_EQ(1, st.Get());
	}
	
	TEST(TStack, IsEmpty_works_correctly)
	{
		TStack<int> st(1);
	
		EXPECT_EQ(1, st.IsEmpty());
	}
	
	TEST(TStack, IsFull_works_correctly)
	{
		TStack<int> st(1);
	
		st.Put(1);
	
		EXPECT_EQ(1, st.IsFull());
	}

Результат их работы продемонстрирован ниже:

![](test_stacks.png)

Пример использования был полностью переработан и теперь выглядит следующим образом:

	void main()
	{
		TStack<int> st(10);
	
		try 
		{
			for (int i = 0; i < 10; i++)
				st.Put(i);
			cout << "Our test stack is" << endl;
			for (int i = 0; i <= 10; i++) // сделана ошибка, вместо < написали <=
				cout << st.Get() << endl;
		}
		catch (int a)
		{
			cout << "Error code:" << st.GetRetCode() << endl; // ошибка поймана!
		}
	}

Результат его работы:

![](main.png)

### 3.5 Проверка правильности введенного арифметического выражения

#### 3.5.1 Описание алгоритма

На вход алгоритма поступает строка символов, на выходе выдается таблица соответствия номеров открывающихся и закрывающихся скобок и общее количество ошибок. Идея алгоритма, решающего поставленную задачу, состоит в следующем.

- Выражение просматривается посимвольно слева направо. Все символы, кроме скобок, игнорируются (т.е. просто производится переход к просмотру следующего символа).
 
- Если очередной символ – открывающая скобка, то её порядковый номер помещается в стек.

- Если очередной символ – закрывающая скобка, то производится выталкивание из стека номера открывающей скобки и запись этого номера в паре с номером закрывающей скобки в результирующую таблицу.

- Если в этой ситуации стек оказывается пустым, то вместо номера открывающей скобки записывается 0, а счетчик ошибок увеличивается на единицу.

- Если после просмотра всего выражения стек оказывается не пустым, то выталкиваются все оставшиеся номера открывающих скобок и записываются в результирующий массив в паре с 0 на месте номера закрывающей скобки, счетчик ошибок каждый раз увеличивается на единицу.

#### 3.5.2 Код функции

Код функции проверки на правильность введенного арифметического выражения

	void IsCorrect(string expr)
	{
		TStack<int> st(20, 10);
		int size = expr.length();
		int numOfErrors = 0;
	
		if (size <= 0)
			throw 1;
	
		cout << "Brackets" << endl;
		cout << "Opening" << " " << "Closing" << endl;
	
		for (int i = 0; i < size; i++)
		{
			if (expr[i] == '(')
				st.Put(i + 1);
			if (expr[i] == ')')
			{
				if (st.IsEmpty())
				{
					numOfErrors++;
					cout << '-' << "       " << i + 1 << endl;
				}
				else
					cout << st.Get() << "       " << i + 1 << endl;
			}
		}
	
		while (!st.IsEmpty())
		{
			cout << st.Get() << "       " << '-' << endl;
			numOfErrors++;
		}
	
		cout << "Number of errors: " << numOfErrors << endl;
	}

Пример работы данной функции для выражения 
``` 
(a+b1)/2+6.5)*(4.8+sin(x)
```
продемонстрирован ниже

![](test_iscorrect.png)

### 3.6 Вычисление математических выражений

#### 3.6.1 Перевод из одной формы в другую


В рамках данного задания требуется разработать алгоритм и составить программу для перевода арифметического выражения из инфиксной формы записи в постфиксную. Инфиксная форма записи характеризуется наличием знаков операций между операндами. Например,

```
(1+2)/(3+4*6.7)-5.3*4.4
```


При такой форме записи порядок действий определяется расстановкой скобок и приоритетом операций. Постфиксная форма записи не содержит скобок, а знаки операций следуют после соответствующих операндов. Тогда для приведённого примера постфиксная форма будет иметь вид:

```
1 2+ 3 4 6.7*+/ 5.3 4.4* -
```


Так как при такой записи несколько операндов могут следовать подряд, то при выводе они разделяются пробелами.
Как результат программа должна напечатать постфиксную форму выражения или выдать сообщение о невозможности построения такой формы в случае обнаружения ошибок при расстановке скобок. 


#### 3.6.2 Вычисление выражения в постфиксной форме


Для выполнения данного задания необходимо разработать алгоритм и составить программу для вычисления арифметического выражения. Программа должна напечатать результат вычисления выражения или выдать сообщение о наличии нечисловых операндов.


#### 3.6.3 Условия и ограничения


При выполнении лабораторной работы могут быть использованы следующие основные допущения:


- Можно предполагать, что арифметические выражения состоят не более чем из 255 символов.
- В качестве допустимых арифметических операций можно рассматривать только символы + (сложение), - (вычитание), * (умножение), / (деление).

#### 3.6.4 Алгоритм решения

##### 3.6.4.1 Проверка скобок


На вход алгоритма поступает строка символов, на выходе должна быть выдана таблица соответствия номеров открывающихся и закрывающихся скобок и общее количество ошибок. Идея алгоритма, решающего поставленную задачу, состоит в следующем.


- Выражение просматривается посимвольно слева направо. Все символы, кроме скобок, игнорируются (т.е. просто производится переход к просмотру следующего символа).
- Если очередной символ – открывающая скобка, то её порядковый номер помещается в стек.
- Если очередной символ – закрывающая скобка, то производится выталкивание из стека номера открывающей скобки и запись этого номера в паре с номером закрывающей скобки в результирующую таблицу.
- Если в этой ситуации стек оказывается пустым, то вместо номера открывающей скобки записывается 0, а счетчик ошибок увеличивается на единицу.
- Если после просмотра всего выражения стек оказывается не пустым, то выталкиваются все оставшиеся номера открывающих скобок и записываются в результирующий массив в паре с 0 на месте номера закрывающей скобки, счетчик ошибок каждый раз увеличивается на единицу.

##### 3.6.4.2 Перевод в постфиксную форму

Данный алгоритм основан на использовании стека.
На вход алгоритма поступает строка символов, на выходе должна быть получена строка с постфиксной формой.
Каждой операции и скобкам приписывается приоритет.

- ( - 0

- ) - 1

- +- - 2

- */ - 3


Предполагается, что входная строка содержит синтаксически правильное выражение.


Входная строка просматривается посимвольно слева направо до достижения конца строки. Операндами будем считать любую последовательность символов входной строки, не совпадающую со знаками определённых в таблице операций. Операнды по мере их появления переписываются в выходную строку. При появлении во входной строке операции, происходит вычисление приоритета данной операции. Знак данной операции помещается в стек, если:

- Приоритет операции равен 0 (это « ( » ),
- Приоритет операции строго больше приоритета операции, лежащей на вершине стека,
- Стек пуст.

В противном случае из стека извлекаются все знаки операций с приоритетом больше или равным приоритету текущей операции. Они переписываются в выходную строку, после чего знак текущей операции помещается в стек.
Имеется особенность в обработке закрывающей скобки. Появление закрывающей скобки во входной строке приводит к выталкиванию и записи в выходную строку всех знаков операций до появления открывающей скобки. Открывающая скобка из стека выталкивается, но в выходную строку не записывается. Таким образом, ни открывающая, ни закрывающая скобки в выходную строку не попадают.
После просмотра всей входной строки происходит последовательное извлечение всех элементов стека с одновременной записью знаков операций, извлекаемых из стека, в выходную строку.

##### 3.6.4.3 Вычисление


Алгоритм вычисления арифметического выражения за один просмотр входной строки основан на использовании постфиксной формы записи выражения и работы со стеком

Выражение просматривается посимвольно слева направо. При обнаружении операнда производится перевод его в числовую форму и помещение в стек (если операнд не является числом, то вычисление прекращается с выдачей сообщения об ошибке.) При обнаружении знака операции происходит извлечение из стека двух значений, которые рассматриваются как операнд2 и операнд1 соответственно, и над ними производится обрабатываемая операция. Результат этой операции помещается в стек. По окончании просмотра всего выражения из стека извлекается окончательный результат.

#### 3.6.5 Реализация алгоритма вычисления значения выражения

Код *expressions.h*

	#ifndef _EXPRESSIONS_
	#define _EXPRESSIONS_
	
	#include <string>
	using namespace std;
	
	double CalcOfExpr(string expr); // функция, производящая непосредственное вычисление выражения по его постфиксной форме
	int IsCorrect(string expr); // функция проверки на правильность расстановки скобок
	string InfixToPolish(string expr); //функция, переводящая выражение из инфиксной формф в постфиксную
	int GetOperationPrio(char c); // функция, возвращающая приоритет операции
	
	#endif

Код *expressions.cpp*

	int GetOperationPrio(char c)
	{
		if (c == '(')
			return 0;
		else if (c == ')')
			return 1;
		else if (c == '+' || c == '-')
			return 2;
		else if (c == '*' || c == '/')
			return 3;
		else
			return -1;
	}
	
	double CalcOfExpr(string expr)
	{
		TStack<double> st(20, 10);
		int size = expr.length(), pos = 0;
		double op1, op2;
		char ch, operandStr[20];
	
		if (size <= 0)
			throw 3;
	
		expr = InfixToPolish(expr);
		size = expr.length();
	
		for (int i = 0; i < size; i++)
		{
			ch = expr[i];
	
			if (ch == '+' || ch == '-' || ch == '*' || ch == '/' || ch == ' ')
				if (operandStr[0] != '\0')
				{
					st.Put(atof(operandStr));
					pos = 0;
					operandStr[0] = '\0';
				}
	
			if (ch == '+')
			{
				op1 = st.Get();
				op2 = st.Get();
				st.Put(op2 + op1);
			}
			else if (ch == '-')
			{
				op1 = st.Get();
				op2 = st.Get();
				st.Put(op2 - op1);
			}
			else if (ch == '*')
			{
				op1 = st.Get();
				op2 = st.Get();
				st.Put(op2 * op1);
			}
			else if (ch == '/')
			{
				op1 = st.Get();
				op2 = st.Get();
				st.Put(op2 / op1);
			}
			else if (ch != ' ')
			{
				operandStr[pos++] = ch;
				operandStr[pos] = '\0';
			}
	
		}
		return st.Get();
	}
	
	string InfixToPolish(string infixExpr)
	{
		TStack<char> st(20, 10);
		bool IsOperand = false;
		int pos = 0, size = infixExpr.length();
		char ch, temp;
		string polishExpr = "";
	
		for (int i = 0; i < size; i++)
		{
			ch = infixExpr[i];
	
			if (ch == '(')
				st.Put(ch);
			else if (ch == ')')
				while (1)
				{
					temp = st.Get();
					if (temp == '(')
						break;
					polishExpr += temp;
				}
			else if (ch == '+' || ch == '-' || ch == '*' || ch == '/')
			{
				//if (IsOperand)
				//{
					polishExpr += ' ';
					//IsOperand = false;
				//}
				if (st.IsEmpty())
					st.Put(ch);
				else
				{
					temp = st.Get();
					if (GetOperationPrio(ch) > GetOperationPrio(temp))
					{
						st.Put(temp);
						st.Put(ch);
					}
					else
					{
						polishExpr += temp;
						while (!st.IsEmpty())
						{
							temp = st.Get();
							if (GetOperationPrio(temp) >= GetOperationPrio(ch))
								polishExpr += temp;
							else
							{
								st.Put(temp);
								break;
							}
						}
						st.Put(ch);
					}
				}
			}
			else
			{
				IsOperand = true;
				polishExpr += ch;
			}
		}
	
		while (!st.IsEmpty())
			polishExpr += st.Get();
	
		return polishExpr;
	}

#### 3.6.6 Тестирование функций проверки правильности расстановки скобок и вычисления выражений

Для функций IsCorrect и CalcOfExpr были написаны следующие тесты:

	TEST(IsCorrect, no_errors_if_expression_is_correct)
	{
		string expr = "((5+3)*7)/(34+5)*(78-45)";
	
		EXPECT_EQ(0, IsCorrect(expr));
	}
	
	TEST(IsCorrect, show_correct_number_of_errors)
	{
		string expr1 = "((5+3*7)/)((34+5)*(78-45)";
	
		string expr2 = "((((";
	
		EXPECT_EQ(1, IsCorrect(expr1) == 1 && IsCorrect(expr2) == 4);
	}
	
	TEST(CalcOfExpr, expression_calculates_correctly)
	{
		double res = (1 + 2) / (3 + 4 * 6.7) - 5.3 * 4.4;
		string expr = "(1+2)/(3+4*6.7)-5.3*4.4";
	
		EXPECT_EQ(res, CalcOfExpr(expr));
	}

Результат их работы продемонстрирован ниже

![](test_expressions.png)

## 4.Заключение

В результате выполнения самостоятельной работы была освоена структура "стек", которая была реализована двумя вариантами: довольно простой *TSimpleStack*, и немного сложнее - *TStack*. Также была рассмотрена одна из областей применения стеков - преобразование и вычисление выражений. 

В данной работе все тесты писались и подключались самостоятельно, что значительно прибавило опыта в работе с фреймворком Google Test.