#ifndef __TSIMPLESTACK_H__
#define	__TSIMPLESTACK_H__

#include <iostream>
using namespace std;

#define MemSize 25
typedef double ValType;

class TSimpleStack
{
private:
	ValType Mem[MemSize];   //������ ��� �����
	int Top;				//������ ������� �����
public:
	TSimpleStack()
	{
		Top = -1;
	}

	bool IsEmpty(void) const	//�������� �������
	{
		return Top == -1;
	}

	bool IsFull(void) const		//�������� ������������
	{
		return Top == MemSize - 1;
	}

	void Put(const ValType val)	//��������� ��������
	{
		if (IsFull())
			throw "Stack is full";
		else
			Mem[++Top] = val;
	}
	ValType Get(void)				//��������� ��������
	{
		if (IsEmpty())
			throw "Stack is empty";
		else
			return Mem[Top--];
	}
};

#endif