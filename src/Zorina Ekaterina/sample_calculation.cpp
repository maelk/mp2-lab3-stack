#include "TStack.h"
#include <iostream>
#include "calculation.h"
using namespace std;

void main()
{
	try
	{	
		string str = "(1+2)/(3+4*6.7)-5.3*4.4";
		cout << "Your string:" << str << endl;
		cout << "Answer = " << Calculation(str) << endl;
	}
	catch (int err)
	{
		cout << err << endl;
	}
}