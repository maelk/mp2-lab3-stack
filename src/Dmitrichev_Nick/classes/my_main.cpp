#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include "tstack.h"
#include "func.h"
#include <cstdio>
#include <string>

using namespace std;
int main()
{
	string str;
	getline(cin,str);
	cout <<"\nYour string:\n"<< str << endl;
	cout << "\nBrackets\nOpen	Close\n";
	CheckBrackets(str);
	cout << "\nIt's your postfix string:\n" << InfiToPost(str) << endl;
	cout <<"Result: "<< Calculate(str) << endl;
	return 0;
}