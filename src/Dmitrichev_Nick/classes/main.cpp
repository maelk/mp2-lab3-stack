// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// Copyright (c) Гергель В.П. 28.07.2000
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (21.04.2015)
//
// Динамические структуры данных - тестирование стека

#include <iostream>
#include "tstack.h"

using namespace std;

void main()
{
  TStack<int> st(2);
  int temp;

  cout << "Testing of programs of support of stacks" << endl;
  for (int i = 0; i < 35; i++)
  {
    st.Put(i);
    cout << "Put the value	" << i << "	Code	" << st.GetRetCode() << endl;
  }
  while (!st.IsEmpty())
  {
    temp = st.Get();
    cout << "Got the value	" << temp << "	Code	" << st.GetRetCode() << endl;
  }
}
