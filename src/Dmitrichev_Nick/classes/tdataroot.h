// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tdataroot.h - Copyright (c) Гергель В.П. 28.07.2000 (06.08)
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (21.04.2015)
//
// Динамические структуры данных - базовый (абстрактный) класс - версия 3.2
//   память выделяется динамически или задается методом SetMem

#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include "tdatacom.h"

#define DefMemSize   25  // размер памяти по умолчанию

#define DataEmpty  -101  // СД пуста
#define DataFull   -102  // СД переполнена
#define DataNoMem  -103  // нет памяти

enum TMemType { MEM_HOLDER, MEM_RENTER };

template<class ValType>
class TDataRoot : public TDataCom
{
protected:
	ValType* pMem;    // память для СД
	int MemSize;      // размер памяти для СД
	int DataCount;    // количество элементов в СД
	TMemType MemType; // режим управления памятью

	void SetMem(void *p, int Size)// *p содержит адрес 'block of new memory'
	// Size - новый размер для 'block of MEM_HOLDER' или 'block of MEM_RENTER'
	{
		if (Size<=0)
			SetRetCode(DataErr);

		else if (MemType == MEM_HOLDER)// выделить новую память и перезаписать
		{
			if (Size > 0)
			{
				ValType *pTempMem = pMem;
				pMem = new ValType[Size];
				for (int i = 0;i < DataCount;++i)
					pMem[i] = *(pTempMem + i);
				MemSize = Size;
				delete[]pTempMem;

				SetRetCode(DataOK);
			}
		}
		else if (MemType==MEM_RENTER)// махинации с MEM_RENTER
		{//p - указатель на новый блок памяти размера Size
			MemSize = Size;
			for (int i = 0;i < DataCount;++i)
				*((ValType*)(ValType*)p+i) = pMem[i];
			pMem = (ValType*)p;

			SetRetCode(DataOK);
		}
		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}
public:
	virtual ~TDataRoot()
	{
		if (MemType == MEM_HOLDER)
			delete pMem;
		pMem = nullptr;
	}

	TDataRoot(int Size = DefMemSize):TDataCom()
	{
		DataCount = 0;
		if (Size == 0)
		{
			MemSize = 0;
			pMem = nullptr;
			MemType = MEM_RENTER;

			SetRetCode(DataOK);
		}
		else if (Size > 0)
		{
			MemSize = Size;
			pMem = new ValType[MemSize];
			MemType = MEM_HOLDER;

			SetRetCode(DataOK);
		}
		else
			//для всех отрицательных Size
			SetRetCode(DataErr);

		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}

	virtual bool IsEmpty(void) const	// контроль пустоты СД
	{
		return DataCount == 0;
	}
	virtual bool IsFull(void) const			// контроль переполнения СД
	{
		return DataCount == MemSize;
	}
	virtual void  Put(const ValType &Val) = 0;	// добавить значение
	virtual ValType Get(void) = 0;				// извлечь значение

	// служебные методы
	virtual int  IsValid() = 0;			// тестирование структуры
	virtual void Print() = 0;			// печать значений

	// дружественные классы
	friend class TMultiStack;
	friend class TSuperMultiStack;
	friend class TComplexMultiStack;
};

#endif
