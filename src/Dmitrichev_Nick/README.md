﻿# Отчёт

## по самостоятельной работе №3 по дисциплине "Алгоритмы и Структуры Данных"

# тема:  "Стек"

## Введение

Лабораторная работа направлена на практическое освоение динамической структуры данных Стек. В качестве области приложений выбрана тема вычисления арифметических
выражений, возникающей при трансляции программ на языке программирования высокого уровня в исполняемые программы.

При вычислении произвольных арифметических выражений возникают две основные задачи: проверка корректности введённого выражения и выполнение операций в порядке, 
определяемом их приоритетами и расстановкой скобок. Существует алгоритм, позволяющий реализовать вычисление произвольного арифметического выражения за один просмотр
без хранения промежуточных результатов. Для реализации данного алгоритма выражение должно быть представлено в постфиксной форме. Рассматриваемые в данной лабораторной
работе алгоритмы являются начальным введением в область машинных вычислений.

## Цели и задачи

### Цель:

С помощью класса `TStack` необходимо написать приложение, которое вычисляет арифметическое выражение, заданное в виде строки и введённое пользователем.

### Задачи:

1. Разработка класса `TSimpleStack` на основе массива фиксированной длины.
1. Реализация методов класса `TDataRoot` согласно заданному интерфейсу.
1. Разработка класса `TStack`, являющегося производным классом от `TDataRoot`.
1. Разработка тестов для проверки работоспособности стеков.
1. Реализация алгоритма проверки правильности введенного арифметического выражения.
1. Реализация алгоритмов разбора и вычисления арифметического выражения.
1. Обеспечение работоспособности тестов и примера использования.

## Перед выполнением работы были получены:

- Интерфейсы классов `TDataCom` и `TDataRoot` (h-файлы);
- Тестовый пример использования класса `TStack`.

#### Для этих файлов:

ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП

`tdataroot.h`, `tdatacom.h`, `main.cpp` - Copyright (c) Гергель В.П. 28.07.2000 (06.08)
Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (21.04.2015)

## Рекомендации:

В процессе выполнения лабораторной работы требуется использовать систему контроля версий [Git][git] и фрэймворк для разработки автоматических тестов [Google Test][gtest].

## Использованные инструменты:

- Система контроля версий [Git][git];
- Фреймворк для написания автоматических тестов [Google Test][gtest];
- Среда разработки Microsoft Visual Studio (2008 или старше);
- Калькулятор.

[git]:         https://git-scm.com/book/ru/v2
[gtest]:       https://github.com/google/googletest


## Общая структура проекта:

- `gtest` — библиотека Google Test;
- `include` — директория для размещения заголовочных файлов;
- `samples` — директория для размещения тестового приложения;
- `sln` — директория с файлами решений и проектов;
- `src` — директория для размещения исходных кодов (cpp-файлы);
    - `Dmitrichev_Nick` — директория, где размещены мои файлы;
- `test` — директория с модульными тестами и основным приложением;
    инициализирующим запуск тестов;
- `README.md` — информация о проекте.


## Разработка шаблонного класса `TSimpleStack` на основе массива фиксированной длины

#### Суть:

Есть статический массив размера `MemSize` элементов типа `ValType`. Этот массив заполняется ТОЛЬКО с помощью метода `Put`, а получение значения осуществляется методом `Get`. И тот, и другой метод
работает с элементом массива, зависящим от значения `Top`. `Top` - индекс, который в случае `TSimpleStack` принимает значения от `-1` до `MemSize`, и, в случае выхода за эти границы, программой
выбрасывается соответсвующее исключение.

#### Файл `tsimplestack.h` - реализация класса `TSimpleStack`:

```c++
#ifndef __SIMPLESTACK_H__
#define __SIMPLESTACK_H__

//Error 1: "Stack is full."
//Error 2: "Stack is empty"

#define MemSize 25

template<class ValType>
class TSimpleStack
{
protected:
	ValType Mem[MemSize];
	int Top;
public:
	TSimpleStack() { Top = -1; }
	TSimpleStack(const ValType&St):Top(St.Top)
	{
		for (int i = 0;i < Top;++i)
			Mem[i] = St.Mem[i];
	}
	bool IsEmpty(void)const { return Top == -1; }
	bool IsFull(void)const { return Top==MemSize-1; }
	void Put(const ValType Val)
	{
		if (IsFull())
			throw 1;
		else
			Mem[++Top] = Val;
	}
	ValType Get(void)
	{
		if (IsEmpty())
			throw 2;
		else
			return Mem[Top--];
	}
};
#endif
```

## Реализация методов класса `TDataRoot` согласно заданному интерфейсу

##### Заметка:

- Файл `TDataRoot.h` переписан так, чтобы класс `TDataRoot` был шаблонным. Причины этого будут описаны ниже.

#### Суть:

Класс `TDataRoot` организует работу с памятью структур данных (СД). Необходимость в нем появилась после осознания наличия общих "корней" у разных СД (например,
методы `IsFull` и `IsEmpty`, которые возвращают `true` значение в том случае, если СД полностью заполнена и пуста соответсвенно, и `false` в противном случае).
Особенность класса `TDataRoot` в том, что он виртуальный; объекты виртуального класса не создаются, поэтому тесты для `TDataRoot` не реализованы.

#### Файл `tdataroot.h` - реализация класса `TDataRoot`

```c++
#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include "tdatacom.h"

#define DefMemSize   25  // размер памяти по умолчанию

#define DataEmpty  -101  // СД пуста
#define DataFull   -102  // СД переполнена
#define DataNoMem  -103  // нет памяти

enum TMemType { MEM_HOLDER, MEM_RENTER };

template<class ValType>
class TDataRoot : public TDataCom
{
protected:
	ValType* pMem;    // память для СД
	int MemSize;      // размер памяти для СД
	int DataCount;    // количество элементов в СД
	TMemType MemType; // режим управления памятью

	void SetMem(void *p, int Size)// *p содержит адрес 'block of new memory'
	// Size - новый размер для 'block of MEM_HOLDER' или 'block of MEM_RENTER'
	{
		if (Size<=0)
			SetRetCode(DataErr);

		else if (MemType == MEM_HOLDER)// выделить новую память и перезаписать
		{
			if (Size > 0)
			{
				ValType *pTempMem = pMem;
				pMem = new ValType[Size];
				for (int i = 0;i < DataCount;++i)
					pMem[i] = *(pTempMem + i);
				MemSize = Size;
				delete[]pTempMem;

				SetRetCode(DataOK);
			}
		}
		else if (MemType==MEM_RENTER)// махинации с MEM_RENTER
		{//p - указатель на новый блок памяти размера Size
			MemSize = Size;
			for (int i = 0;i < DataCount;++i)
				*((ValType*)(ValType*)p+i) = pMem[i];
			pMem = (ValType*)p;

			SetRetCode(DataOK);
		}
		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}
public:
	virtual ~TDataRoot()
	{
		if (MemType == MEM_HOLDER)
			delete pMem;
		pMem = nullptr;
	}

	TDataRoot(int Size = DefMemSize):TDataCom()
	{
		DataCount = 0;
		if (Size == 0)
		{
			MemSize = 0;
			pMem = nullptr;
			MemType = MEM_RENTER;

			SetRetCode(DataOK);
		}
		else if (Size > 0)
		{
			MemSize = Size;
			pMem = new ValType[MemSize];
			MemType = MEM_HOLDER;

			SetRetCode(DataOK);
		}
		else
			//для всех отрицательных Size
			SetRetCode(DataErr);

		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}

	virtual bool IsEmpty(void) const	// контроль пустоты СД
	{
		return DataCount == 0;
	}
	virtual bool IsFull(void) const			// контроль переполнения СД
	{
		return DataCount == MemSize;
	}
	virtual void  Put(const ValType &Val) = 0;	// добавить значение
	virtual ValType Get(void) = 0;				// извлечь значение

	// служебные методы
	virtual int  IsValid() = 0;			// тестирование структуры
	virtual void Print() = 0;			// печать значений

	// дружественные классы
	friend class TMultiStack;
	friend class TSuperMultiStack;
	friend class TComplexMultiStack;
};

#endif
```

##### Заметка:

- Что такое `SetRetCode` и `GetRetCode` и откуда они взялись? К началу работы уже был получен файл `tdatacom.h` - класс ошибок, который осуществляет установку и возврат
кодов завершения методов. Класс `TDataCom` - предок класса `TDataRoot`. Нужен ли `tdatacom.h`? Мне кажется нет, потому что вместо того, чтобы устанавливать коды 
завершения с помощью методов класса `TDataCom`, можно выбрасывать исключения ТОЛЬКО в случае ошибок. Я принял решение использовать исключения и не забывать при этом о
классе `TDataCom`

#### Файл `tdatacom.h` - класс `TDataCom`

```c++
#ifndef __DATACOM_H__
#define __DATACOM_H__

#define DataOK   0
#define DataErr -1

// TDataCom является общим базовым классом
class TDataCom
{
protected:
  int RetCode; // Код завершения

  int SetRetCode(int ret) { return RetCode = ret; }
public:
  TDataCom(): RetCode(DataOK) {}
  virtual ~TDataCom() = 0 {}
  int GetRetCode()
  {
    int temp = RetCode;
    RetCode = DataOK;
    return temp;
  }
};

#endif
```

## Разработка класса `TStack`, являющегося производным классом от `TDataRoot`

#### Суть:

Класс `TStack` - улучшение класса `TSimpleStack`. Что стало лучше? Пропало верхнее ограничение на `MemSize` элементов - память стала не статической, а динамической.
Очевидно, что в определенные моменты времени, верхняя граница будет (всё та же `MemSize`); но в методе `Put`, вместо того, чтобы выбрасывать исключение, как в
`TSimpleStack`, вызывается метод `SetMem` класса `TDataRoot`, который в моей реализации стека сдвигает верхнюю границу на `DefMemSize` элементов. То есть, выделяет
дополнительно `DefMemSize` элементов. Можно было выделять память не блоками, а поштучно (поэлементно), но это становится причиной падения скорости работы стека.
Метод `SetMem` также используется в методе 'Get' класса `TStack`, удаляя освободившийся блок памяти.

##### Заметка:

- Очевидно, что класс `TStack` должен быть шаблонным, как и его предок `TDataRoot`.

#### Файл `tstack.h` - реализация класса `TStack`

```c++
#ifndef __TSTACK_H__
#define __TSTACK_H__

#include <iostream>
#include "TDataRoot.h"

template <class ValType>
class TStack :public TDataRoot<ValType>
{
private:
	int Hi;// вершина стека
public:
//Конструкторы
	TStack(int Size = DefMemSize) :TDataRoot(Size), Hi(-1){};// конструктор по умолчанию и с параметром
	TStack(const TStack& St)// конструктор копирования
	{
		MemSize = St.MemSize;
		DataCount = St.DataCount;
		Hi = St.Hi;
		pMem = new ValType[MemSize];
		for (int i = 0;i < DataCount;++i)
			pMem[i] = St.pMem[i];

		SetRetCode(DataOK);
	}

//Методы
	void  Put(const ValType &Val)// добавить значение
	{
		if (pMem == nullptr)
			SetRetCode(DataNoMem);
		else if (IsFull())// увеличить стек на DefMemSize элементов, если он полный
		{
			void *p = nullptr;
			SetMem(p, MemSize + DefMemSize);
			pMem[++Hi] = Val;
			DataCount++;

			SetRetCode(DataOK);
		}
		else// обычная вставка элемента
		{
			pMem[++Hi] = Val;
			DataCount++;

			SetRetCode(DataOK);
		}

		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}
	ValType Get()// извлечь значение
	{
		if (pMem == nullptr)
			SetRetCode(DataNoMem);
		else if (IsEmpty())
			SetRetCode(DataEmpty);
		else // получить элемент с вершины стека
		{
			ValType res = pMem[Hi--];
			DataCount--;
			if ((Hi>DefMemSize - 1) && (Hi%DefMemSize == 0)) // если блок памяти размера DefMemSize оказывается пустым
			{
				void *p = nullptr;
				SetMem(p, MemSize - DefMemSize);
			}
			SetRetCode(DataOK);
			return res;
		}

		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}
	ValType GetHighElem()// получить значение с вершины, не извлекая из стека
	{
		if (pMem == nullptr)
			SetRetCode(DataNoMem);
		else if (IsEmpty())
			SetRetCode(DataEmpty);
		else
		{
			SetRetCode(DataOK);
			return pMem[Hi];
		}

		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}

	virtual void Print()// печать значений
	{
		if (pMem != nullptr)
		{
			for (int i = 0;i < DataCount;++i)
				std::cout << pMem[i] << ' ';
			printf("\n");

			SetRetCode(DataOK);
		}
		else
			throw SetRetCode(DataNoMem);

		if (SetRetCode(GetRetCode()) != DataOK)
			throw GetRetCode();
	}
protected:
	int  IsValid()// тестирование структуры
	{
		int res = 0;
		if (pMem == nullptr)
			res++;
		if (MemSize < DataCount)
			res += 2;
		return res;
	}
};

#endif
```

##### Заметки:

- Тестирование структуры - `IsValid` проверяет объект класса `TStack` на наличие серьезных ошибок (нулевой указатель и выход за границы);
- `GetHighElem` в обычном стеке мог бы и не пригодится, но в моем случае он нужен для реализации метода обработки арифметического выражения (будет ниже);
- Из этой реализации видно, что `TSimpleStack` и `TStack` очень похожи.

## Разработка тестов для проверки работоспособности стеков

##### Заметка:

Что должны проверять тесты для классов `TSimpleStack` и `TStack`? В первую очередь очевидные заключения:

- Созданный стек является пустым;
- Стек, в который выполнена вставка, не пуст;
- Стек после операции исключения не полон;
- При выборке значения из стека извлекается последнее вставленное значение;
- Выборка значения из пустого стека выбрасывает исключение;
- Вставка значения в полный стек выбрасывает исключения (для `TSimpleStack`)

#### Файл `test_tsimplestack.cpp` - тестирование класса `TSimpleStack`

```c++
#include "TSimpleStack.h"
#include <cstdio>
#include <gtest\gtest.h>

TEST(TSimpleStack, created_stack_is_empty)
//только что созданный стек пустой
{
	TSimpleStack<int> st;

	EXPECT_TRUE(st.IsEmpty());
}

TEST(TSimpleStack, created_stack_is_not_nullptr)
//только что созданный стек имеет свой блок памяти
{
	TSimpleStack<int> st;

	EXPECT_NE(&st, nullptr);
}

TEST(TSimpleStack, stack_in_which_was_inserted_an_element_is_not_empty)
//стек, в который производится вставка элемента, не пустой
{
	TSimpleStack<int> st;
	st.Put(0);

	EXPECT_FALSE(st.IsEmpty());
}

TEST(TSimpleStack, stack_from_which_was_got_an_element_is_not_full)
//стек, из которого извлекли элемент, неполный
{
	TSimpleStack<int> st;
	for (int i = 0;i < MemSize;++i)
		st.Put(0);
	st.Get();

	EXPECT_FALSE(st.IsFull());
}

TEST(TSimpleStack, stack_returns_last_put_element)
//стек возвращает последний вставленный элемент
{
	TSimpleStack<int> st;
	st.Put(5);

	EXPECT_EQ(5, st.Get());
}

TEST(TSimpleStack, cant_get_from_empty_stack)
//нельзя извлечь элемент из пустого стека
{
	TSimpleStack<int> st;

	EXPECT_ANY_THROW(st.Get());
}

TEST(TSimpleStack, cant_put_in_full_stack)
//нельзя вставить элемент в полный стек
{
	TSimpleStack<int> st;
	for (int i = 0; i < MemSize; ++i)
		st.Put(0);

	EXPECT_ANY_THROW(st.Put(0));
}

TEST(TSimpleStack, can_copy_stack)
//можно скопировать стек
{
	TSimpleStack<int> st1;
	for (int i = 0; i < MemSize; ++i)
		st1.Put(0);

	EXPECT_NO_THROW(TSimpleStack<int> st2(st1));
}

TEST(TSimpleStack, copied_stack_is_equil_to_source_one)
//скопированный стек равен оригиналу
{
	TSimpleStack<int> st1;
	for (int i = 0; i < MemSize; ++i)
		st1.Put(0);
	TSimpleStack<int> st2(st1);
	bool result = true;
	for (int i = 0; i < MemSize; ++i)
		if (st1.Get() != st2.Get())
		{
			result = false;
			break;
		}

	EXPECT_TRUE(result);
}

TEST(TSimpleStack, copied_stack_has_its_own_memory)
//у скопированного стека свой адрес
{
	TSimpleStack<int> st1;
	for (int i = 0;i < MemSize;++i)
		st1.Put(0);
	TSimpleStack<int> st2(st1);

	EXPECT_NE(&st1, &st2);
}
```

##### Консоль:

![](http://images.vfl.ru/ii/1481813772/88726b3a/15345031.png)

#### Файл `test_tstack.cpp` - тестирование класса `TStack`

```c++
#include "tstack.h"
#include <cstdio>
#include <gtest\gtest.h>


TEST(TStack, created_stack_is_empty)
//только что созданный стек пустой
{
	TStack<int> st;

	EXPECT_TRUE(st.IsEmpty());
}

TEST(TStack, created_stack_is_not_nullptr)
//только что созданный стек имеет свой блок памяти
{
	TStack<int> st;

	EXPECT_NE(&st, nullptr);
}

TEST(TStack, stack_in_which_was_inserted_an_element_is_not_empty)
//стек, в который производится вставка элемента, не пустой
{
	TStack<int> st;
	st.Put(0);

	EXPECT_FALSE(st.IsEmpty());
}

TEST(TStack, stack_from_which_was_got_an_element_is_not_full)
//стек, из которого извлекли элемент, неполный
{
	TStack<int> st;
	for (int i = 0;i < DefMemSize;++i)
		st.Put(0);
	st.Get();

	EXPECT_FALSE(st.IsFull());
}

TEST(TStack, last_put_element_is_high_of_stack)
//последний вставленный элемент - элемент на вершине стека
{
	TStack<int> st;
	st.Put(0);

	EXPECT_EQ(st.Get(),st.GetHighElem());
}

TEST(TStack, stack_returns_last_put_element)
//стек возвращает последний вставленный элемент
{
	TStack<int> st;
	st.Put(5);

	EXPECT_EQ(5, st.Get());
}

TEST(TStack, cant_get_from_empty_stack)
//нельзя извлечь элемент из пустого стека
{
	TStack<int> st;

	EXPECT_ANY_THROW(st.Get());
}

TEST(TStack, can_put_in_stack_with_DefMemSize_size)
//можно вставить элемент, если стек псевдозаполненный
{
	TStack<int> st;
	for (int i = 0; i < DefMemSize; ++i)
		st.Put(0);

	EXPECT_NO_THROW(st.Put(0));
}

TEST(TStack, stack_is_full_only_when_DataCount_is_equil_DefMemSize_but_it_increases)
//стек псевдозаполнен только тогда, когда DataCount=DefMemSize
{
	TStack<int> st;
	int result = 0;
	for (int i = 0;i < 4 * DefMemSize;++i)
	{
		st.Put(i);
		if (st.IsFull())
			result++;
	}

	EXPECT_EQ(result,4);
}

TEST(TStack, can_copy_stack)
//можно скопировать стек
{
	TStack<int> st1;
	for (int i = 0; i < DefMemSize; ++i)
		st1.Put(0);

	EXPECT_NO_THROW(TStack<int> st2(st1));
}

TEST(TStack, copied_stack_is_equil_to_source_one)
//скопированный стек равен оригиналу
{
	TStack<int> st1;
	for (int i = 0; i < DefMemSize; ++i)
		st1.Put(i);
	TStack<int> st2(st1);
	bool result = true;
	for (int i = 0; i < DefMemSize; ++i)
		if (st1.Get() != st2.Get())
		{
			result = false;
			break;
		}

	EXPECT_TRUE(result);
}

TEST(TStack, copied_stack_has_its_own_memory)
//у скопированного стека свой адрес
{
	TStack<int> st1;
	for (int i = 0;i < DefMemSize;++i)
		st1.Put(0);
	TStack<int> st2(st1);

	EXPECT_NE(&st1, &st2);
}
```

##### Консоль:

![](http://images.vfl.ru/ii/1481813998/b4d8a8a1/15345061.png)

## Реализация алгоритма проверки правильности введенного арифметического выражения и реализация алгоритмов разбора и вычисления арифметического выражения.

### Определения и описание:

Арифметическое выражение - выражение, в котором операндами являются объекты, над которыми выполняются арифметические операции. Например,

```
(1+2)/(3+4*6.7)-5.3*4.4
```

При такой форме записи (называемой инфиксной, где знаки операций стоят между операндами) порядок действий определяется расстановкой скобок и приоритетом операций. Постфиксная (или обратная польская) форма записи не содержит скобок, а знаки операций следуют после соответствующих операндов. Тогда для приведённого примера постфиксная форма будет иметь вид:

```
1 2+ 3 4 6.7*+/ 5.3 4.4* -
```

Обратная польская нотация была разработана австралийским ученым Чарльзом Хэмблином в середине 50-х годов прошлого столетия на основе польской нотации, которая была предложена в 1920 году польским математиком Яном Лукасевичем. Эта нотация лежит в основе организации вычислений для арифметических выражений. Известный ученый Эдсгер Дейкстра предложил алгоритм для перевода выражений из инфиксной в постфиксную форму. Данный алгоритм основан на использовании стека.


#### Суть:

На вход подается строка, являющаяся арифметическим выражением или неявляющимся таковым (во втором нет смысла, но проверить надо).

##### Заметки:

Что может быть не так?

- Расстановка скобок;
- Недопустимые символы;
- Лишние и ненужные пробелы.

### Контроль над расстановкой скобок

В рамках данной лабораторной работы предлагается ограничить контроль только правильной расстановкой скобок . Таким образом, требуется проанализировать соответствие открывающих и закрывающих круглых скобок во введённом арифметическом выражении. Программа должна напечатать таблицу соответствия скобок, причем в таблице должно быть указано, для каких скобок отсутствуют парные им, а также общее количество найденных ошибок. Для идентификации скобок могут быть использованы их порядковые номера в выражении. Например, для арифметического выражения 


```
1   2      3  4       5 6
(a+b1)/2+6.5)*(4.8+sin(x)
```

должна быть напечатана таблица вида:

![](http://images.vfl.ru/ii/1481815771/e4782065/15345541.png)

Прочерки в таблице обозначают отсутствие соответствующей скобки. При отсутствии обнаруженных ошибок программа должна выдать соответствующее сообщение.

### Алгоритм проверки скобок (реализован в файле `func.cpp` и называется `CheckBrackets`):

На вход алгоритма поступает строка символов, на выходе должна быть выдана таблица соответствия номеров открывающихся и закрывающихся скобок и общее количество ошибок. Идея алгоритма, решающего поставленную задачу, состоит в следующем.

- Выражение просматривается посимвольно слева направо. Все символы, кроме скобок, игнорируются (т.е. просто производится переход к просмотру следующего символа).
- Если очередной символ – открывающая скобка, то её порядковый номер помещается в стек.
- Если очередной символ – закрывающая скобка, то производится выталкивание из стека номера открывающей скобки и запись этого номера в паре с номером закрывающей скобки в результирующую таблицу.
- Если в этой ситуации стек оказывается пустым, то вместо номера открывающей скобки записывается 0, а счетчик ошибок увеличивается на единицу.
- Если после просмотра всего выражения стек оказывается не пустым, то выталкиваются все оставшиеся номера открывающих скобок и записываются в результирующий массив в паре с 0 на месте номера закрывающей скобки, счетчик ошибок каждый раз увеличивается на единицу.

### Перевод из одной формы в другую:

В рамках данного задания требуется разработать алгоритм и составить программу для перевода арифметического выражения из инфиксной формы записи в постфиксную. Инфиксная форма записи характеризуется наличием знаков операций между операндами. Например,

```
(1+2)/(3+4*6.7)-5.3*4.4
```


При такой форме записи порядок действий определяется расстановкой скобок и приоритетом операций. Постфиксная форма записи не содержит скобок, а знаки операций следуют после соответствующих операндов. Тогда для приведённого примера постфиксная форма будет иметь вид:

```
1 2+ 3 4 6.7*+/ 5.3 4.4* -
```


Так как при такой записи несколько операндов могут следовать подряд, то при выводе они разделяются пробелами.
Как результат программа должна напечатать постфиксную форму выражения или выдать сообщение о невозможности построения такой формы в случае обнаружения ошибок при расстановке скобок. 

### Алгоритм перевода выражения в постфиксную форму (реализован в файле `func.cpp` и называется `InfiToPost`):

Данный алгоритм основан на использовании стека.
На вход алгоритма поступает строка символов, на выходе должна быть получена строка с постфиксной формой.
Каждой операции и скобкам приписывается приоритет.

- ( - 0

- ) - 1

- +- - 2

- */ - 3


Предполагается, что входная строка содержит синтаксически правильное выражение.


Входная строка просматривается посимвольно слева направо до достижения конца строки. Операндами будем считать любую последовательность символов входной строки, не совпадающую со знаками определённых в таблице операций. Операнды по мере их появления переписываются в выходную строку. При появлении во входной строке операции, происходит вычисление приоритета данной операции. Знак данной операции помещается в стек, если:

- Приоритет операции равен 0 (это « ( » ),
- Приоритет операции строго больше приоритета операции, лежащей на вершине стека,
- Стек пуст.

В противном случае из стека извлекаются все знаки операций с приоритетом больше или равным приоритету текущей операции. Они переписываются в выходную строку, после чего знак текущей операции помещается в стек.
Имеется особенность в обработке закрывающей скобки. Появление закрывающей скобки во входной строке приводит к выталкиванию и записи в выходную строку всех знаков операций до появления открывающей скобки. Открывающая скобка из стека выталкивается, но в выходную строку не записывается. Таким образом, ни открывающая, ни закрывающая скобки в выходную строку не попадают.
После просмотра всей входной строки происходит последовательное извлечение всех элементов стека с одновременной записью знаков операций, извлекаемых из стека, в выходную строку.

### Алгоритм вычисления значения выражения в постфиксной форме (реализован в файле `func.cpp` и называется `CalcPost`):

Алгоритм вычисления арифметического выражения за один просмотр входной строки основан на использовании постфиксной формы записи выражения и работы со стеком

Выражение просматривается посимвольно слева направо. При обнаружении операнда производится перевод его в числовую форму и помещение в стек (если операнд не является числом, то вычисление прекращается с выдачей сообщения об ошибке.) При обнаружении знака операции происходит извлечение из стека двух значений, которые рассматриваются как операнд2 и операнд1 соответственно, и над ними производится обрабатываемая операция. Результат этой операции помещается в стек. По окончании просмотра всего выражения из стека извлекается окончательный результат.

#### Мои ограничения:

- Использование таких функций, как `sin()`, `cos()`, `ln()` и других, не предусмотрено моей работой, поэтому если таковые есть в строке, будет выброшено исключение.
- Можно предполагать, что арифметические выражения состоят не более чем из 255 символов.
- В качестве допустимых арифметических операций можно рассматривать только символы + (сложение), - (вычитание), * (умножение), / (деление).


##### Заметки:

- Файл `func.h` не содержит в себе классы; но содержит прототипы моих функций и их описание;
- Реализованы вспомогательные функции.

#### Файл `func.h` - прототипы и описание моих функций

```c++
#ifndef __FUNC_H__
#define __FUNC_H__


#include <string>
#include "tstack.h"

bool CheckSymbols(std::string&);/*
Проверка строки на наличие символов, не входящих во множество допустимых символов*/

void DeleteSpace(std::string&);/*
Удаление пробелов из строки*/

int CheckBrackets(std::string&, bool=true);/*
Проверка скобок. Первый параметр - входная строка.
Второй параметр - выводить или не выводить таблицу номеров скобок.
1- выводить. 0- не выводить.
*/

int GetPrio(char);/*
Получить приоритет операции
*/

std::string InfiToPost(std::string&);/*
Перевод арифметического выражения инфиксного вида
в постфиксный. Выполняется с помощью
предложенного алгоритма со стеком
*/

double CalcBin(double, double, char);/*
Подсчет значения бинарной операции, определяемой char,
и двух операндов double. Порядок записи double важен.
*/

double CalcPost(std::string&);/*
Подсчет значения арифметического выражения, представленного
в постфиксной форме
*/

double Calculate(std::string);/*
Всё вместе*/

#endif
```

##### Заметки:

- Класс `TStack` реализовывался именно для использования его в обработке арифметических выражений;
- В функцию `Calculate` передается копия строки, введённой пользователем, а другие функции обращаются уже к адресу этой копии.

### Важная заметка:

Почему же всё-таки шаблоны? Потому что в представленных ниже функциях используются стеки разных типов (`TStack<double>`, `TStack<char>` и т.д.)

#### Файл `func.cpp` - реализация функций(с комментариями)

```c++
#ifndef __FUNC_CPP__
#define __FUNC_CPP__

//Error 1 "Check your string. The line contains an unacceptable symbol. "
//Error 2 "Check your brackets."

#include "func.h"

bool CheckSymbols(std::string &Line)
{
	
	for (std::string::iterator Line_Iter = Line.begin();Line_Iter != Line.end();++Line_Iter)
		if (GetPrio(*Line_Iter) == -3)
			return false;
	return true;
}

void DeleteSpace(std::string& Line)
{
	for (std::string::iterator Line_Iter = Line.begin();Line_Iter != Line.end();++Line_Iter)
		if (*Line_Iter == ' ')
		{
			Line.erase(Line_Iter);
			Line_Iter--;
		}
}

int CheckBrackets(std::string &Line, bool Do_Print)
{
	TStack<int> Bracket;
	int  Count_Open = 1,  Count_Error = 0, Numb_Bracket;

	for (std::string::iterator Line_Iter = Line.begin();Line_Iter != Line.end();++Line_Iter)
	{
		while ((Line_Iter != Line.end())&&(*Line_Iter != '(') && (*Line_Iter != ')')) //пока не встретится скобка
			Line_Iter++;
		if (Line_Iter == Line.end())
			break;

		if (*Line_Iter == '(')
			Bracket.Put(Count_Open++);
		if (*Line_Iter == ')')
		{
			if (Do_Print) //если надо выводить на экран
			{
				if (Bracket.IsEmpty())
				{
					Numb_Bracket = 0;
					Count_Error++;
				}
				else
					Numb_Bracket = Bracket.Get();
					printf("%d	%d\n", Numb_Bracket, Count_Open);
			}
			else
				if (Bracket.IsEmpty())
					Count_Error++;
				else
					Bracket.Get();
				
			Count_Open++;
		}
	}

	if (Do_Print) //если надо выводить на экран
		while (Bracket.IsEmpty()==false)
		{
			Count_Error++;
			printf("%d  %d\n", Bracket.Get(), 0);
		}
	else
		while (Bracket.IsEmpty()==false)
		{
			Count_Error++;
			Bracket.Get();
		}

	if (Bracket.GetRetCode()==DataOK)
		return Count_Error;
}

int GetPrio(char Operation)
{
	if (Operation == '(')
		return 0;
	else if (Operation == ')')
		return 1;
	else if ((Operation == '+') || (Operation == '-'))
		return 2;
	else if ((Operation == '*') || (Operation == '/'))
		return 3;
	else if ((Operation >= '0') && (Operation <= '9') || (Operation == '.')) //если символ - составляющее числа
		return -1;
	else if (Operation == ' ')
		return -2;
	else //символ находится в таблице ASCII и не принадлежит множеству {'(', ')', '+', '-', '*', '/', '.', ' ', '0'...'9'}
		return -3;
};

std::string InfiToPost(std::string &Line)
{
	TStack<char> Operator;
	std::string Post = "";
	int  High_Prio = -1;
	
	for (std::string::iterator Line_Iter = Line.begin();Line_Iter != Line.end();++Line_Iter)
	{
		while (Line_Iter!=Line.end() && GetPrio(*Line_Iter)==-1) //запись числа
		{
			Post += *Line_Iter;
			Line_Iter++;
		}

		if (Line_Iter == Line.end())
			break;

		if ((*Line_Iter == '(')|| (GetPrio(*Line_Iter) > High_Prio)|| (Operator.IsEmpty()))
		//если открывающая скобка || если приор. больше приор. вершины || стек пуст
		{
			if (Operator.IsEmpty() == false)
				Post += ' ';
			Operator.Put(*Line_Iter);
			High_Prio = GetPrio(*Line_Iter);
		}
		else if (*Line_Iter == ')')//если закрывающая скобка
		{
			while (Operator.GetHighElem() != '(')
				Post += Operator.Get();
			Operator.Get();
		}
		else //извлечение из стека всех операций, приор. которых <= приоритету элемента строки
		{
			while ((Operator.IsEmpty()==0)&&(GetPrio(*Line_Iter) <= GetPrio(Operator.GetHighElem())))
				Post+= Operator.Get();
			Operator.Put(*Line_Iter);
			Post+= ' ';
		}	
	}
	while (Operator.IsEmpty() == false)//извлечь из стека все операции, которые остались
		Post+= Operator.Get();
	Post+= '\0';

	if (Operator.GetRetCode() == DataOK)
		return Post;
};

double CalcBin(double Fir, double Sec, char Op)
{
	if (Op == '-')
		return Fir - Sec;
	else if (Op == '+')
		return Fir + Sec;
	else if (Op == '*')
		return Fir * Sec;
	else if (Op == '/')
		return Fir / Sec;
}

double CalcPost(std::string &Line)
{
	TStack<double> Numbers;
	std::string CurrNumbString = "";
	double CurrNumbDouble = 0;

	for (std::string::iterator Line_Iter = Line.begin();Line_Iter != Line.end();++Line_Iter)
	{
		if (GetPrio(*Line_Iter) == -2)//если пробел
			Line_Iter++;
		while ((Line_Iter != Line.end()) && (GetPrio(*Line_Iter) == -1))//запись числа из строки в числовую строку
		{
			CurrNumbString += *Line_Iter;
			Line_Iter++;
		}
		CurrNumbString += '\0';
		if (CurrNumbString[0] != '\0')//перевод числовой строки в double
			Numbers.Put(stod(CurrNumbString));
		CurrNumbString = "";
		if (GetPrio(*Line_Iter) > 0)//если операция
		{
			CurrNumbDouble = CalcBin(Numbers.Get(), Numbers.Get(), *Line_Iter);
			Numbers.Put(CurrNumbDouble);
		}
	}

	if (Numbers.GetRetCode() == DataOK)
		return Numbers.Get();
}

double Calculate(std::string Line)
{
	if (CheckSymbols(Line))
		if (CheckBrackets(Line,0) == 0)
		{
			DeleteSpace(Line);
			return CalcPost(InfiToPost(Line));
		}
		else
			throw 2;//"Check your brackets."
	else
		throw 1;//"Check your string. The line contains an unacceptable symbol."
}
#endif
```

##### Заметка:

- Все функции, принимающие на вход строку, обрабатывают её за один проход слева направо.

#### Файл `test_func.cpp` - тесты для функций обработки выражения

```c++
#include "func.h"
#include <cstdio>
#include <gtest\gtest.h>
#include "func.cpp"

TEST(func, CheckSymbols_returns_true_if_string_is_correct)
{
	std::string st = "1+2";

	EXPECT_TRUE(CheckSymbols(st));
}

TEST(func, CheckSymbols_returns_false_if_string_is_incorrect)
{
	std::string st = "1a2";

	EXPECT_FALSE(CheckSymbols(st));
}

TEST(func, CheckBrackets_returns_0_if_string_not_contains_brackets)
{
	std::string st = "1+2";

	EXPECT_EQ(CheckBrackets(st,0),0);
}

TEST(func, CheckBrackets_returns_0_if_string_contains_correct_brackets)
{
	std::string st = "1+((1+2)*1)";

	EXPECT_EQ(CheckBrackets(st, 0), 0);
}

TEST(func, CheckBrackets_returns_not_0_if_string_contains_incorrect_brackets)
{
	std::string st = "(((()())(";

	EXPECT_EQ(CheckBrackets(st, 0), 3);
}

TEST(func, DeleteSpace_deletes_spaces)
{
	std::string st = "1  1";
	DeleteSpace(st);

	EXPECT_EQ(st, "11");
}

TEST(func, CalcBin_calculates_correctly)
{
	EXPECT_EQ(CalcBin(1,2,'-'), -1);
}

TEST(func, CalcPost_calculates_the_example_correctly)
{
	std::string st = "(1+2)/(3+4*6.7)-5.3*4.4";
	double res_st = (1 + 2) / (3 + 4 * 6.7) - 5.3*4.4;
	st = InfiToPost(st);

	EXPECT_TRUE(0.5>=abs(CalcPost(st)-res_st));
}

TEST(func, Caltulate_can_not_calculate_if_string_contains_an_unacceptable_symbol)
{
	std::string st = "1a2";

	EXPECT_ANY_THROW(Calculate(st));
}

TEST(func, Caltulate_can_not_calculate_if_string_contains_incorrect_brackets)
{
	std::string st = "1)((2";

	EXPECT_ANY_THROW(Calculate(st));
}

TEST(func, Calculate_can_calculate_the_example_correctly)
{
	std::string st = "(1+2)/(3+4*6.7)-5.3*4.4";
	double res_st = (1 + 2) / (3 + 4 * 6.7) - 5.3*4.4;

	EXPECT_TRUE(0.5 >= abs(Calculate(st) - res_st));
}
```

#### Консоль:

![](http://images.vfl.ru/ii/1481816587/0e823d51/15345819.png)

## Обеспечение работоспособности тестов и примера использования

Скриншоты результата запуска тестов были представлены выше. Ошибки не были обнаружены.

#### Файл `main.cpp` - пример использования стека

```c++
#include <iostream>
#include "tstack.h"

using namespace std;

void main()
{
  TStack<int> st(2);
  int temp;

  cout << "Testing of programs of support of stacks" << endl;
  for (int i = 0; i < 35; i++)
  {
    st.Put(i);
    cout << "Put the value	" << i << "	Code	" << st.GetRetCode() << endl;
  }
  while (!st.IsEmpty())
  {
    temp = st.Get();
    cout << "Got the value	" << temp << "	Code	" << st.GetRetCode() << endl;
  }
}
```
#### Консоль:

![](http://images.vfl.ru/ii/1481817175/da4c820f/15345999.png)

#### Файл `my_main.cpp` - пример использования функций обработки арифметического выражения

```c++
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include "tstack.h"
#include "func.h"
#include <cstdio>
#include <string>

using namespace std;
int main()
{
	string str;
	getline(cin,str);
	cout <<"\nYour string:\n	"<< str << endl;
	cout << "\nBrackets\nOpen	Close\n";
	CheckBrackets(str);
	cout << "\nIt's your postfix string:\n	" << InfiToPost(str) << endl;
	cout <<"Result: "<< Calculate(str) << endl;
	return 0;
}
```

#### Консоль и калькулятор:

![](http://images.vfl.ru/ii/1481818020/9cb78cbf/15346155.png)
![](http://images.vfl.ru/ii/1481818259/2980b2ab/15346193.png)

## Вывод:

- Функции обработки арифметических выражений выдают корректный результат;
- Может показаться, что раз класс `TStack` легко реализовывается, то от него не будет пользы на практике; но из выполненной работы видно обратное;
- Наследование функций классами-потомками очень полезная особенность языка C++.
