#ifndef __FUNC_H__
#define __FUNC_H__


#include <string>
#include "tstack.h"

bool CheckSymbols(std::string&);/*
�������� ������ �� ������� ��������, �� �������� �� ��������� ���������� ��������*/

void DeleteSpace(std::string&);/*
�������� �������� �� ������*/

int CheckBrackets(std::string&, bool=true);/*
�������� ������. ������ �������� - ������� ������.
������ �������� - �������� ��� �� �������� ������� ������� ������.
1- ��������. 0- �� ��������.
*/

int GetPrio(char);/*
�������� ��������� ��������
*/

std::string InfiToPost(std::string&);/*
������� ��������������� ��������� ���������� ����
� �����������. ����������� � �������
������������� ��������� �� ������
*/

double CalcBin(double, double, char);/*
������� �������� �������� ��������, ������������ char,
� ���� ��������� double. ������� ������ double �����.
*/

double CalcPost(std::string&);/*
������� �������� ��������������� ���������, ���������������
� ����������� �����
*/

double Calculate(std::string);/*
�� ������*/

#endif
