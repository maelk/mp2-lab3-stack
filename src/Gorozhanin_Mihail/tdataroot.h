#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include "tdatacom.h"

#define DefMemSize   25  

#define DataEmpty  -101  
#define DataFull   -102  
#define DataNoMem  -103  
#define SizeIncorrect -104 

enum TMemType { MEM_HOLDER, MEM_RENTER };

template <class T>
class TDataRoot : public TDataCom
{
protected:
	T* pMem;      
	int MemSize;      
	int DataCount;    
	TMemType MemType; 

	void SetMem(void *p, int Size);      
public:
	virtual ~TDataRoot();
	TDataRoot(int Size = DefMemSize);
	TDataRoot(const TDataRoot&);
	virtual bool IsEmpty(void) const;    
	virtual bool IsFull(void) const;     
	virtual void  Put(const T &Val) = 0; 
	virtual T Get(void) = 0; 

							
	virtual void Print() = 0; 

														
	friend class TMultiStack;
	friend class TSuperMultiStack;
	friend class TComplexMultiStack;
};

template <class T>
TDataRoot<T>::TDataRoot(int s) : TDataCom()
{
	MemSize = s;
	DataCount = 0;

	if (s == 0)
	{
		pMem = nullptr;
		MemType = MEM_RENTER;
	}
	else if (s > 0)
	{
		pMem = new T[MemSize];
		MemType = MEM_HOLDER;
	}
	else
		throw SetRetCode(SizeIncorrect);
}

template <class T>
TDataRoot<T>::TDataRoot(const TDataRoot &dr)
{
	MemType = MEM_HOLDER;
	MemSize = dr.MemSize;
	DataCount = dr.DataCount;

	pMem = new T[MemSize];

	for (int i = 0; i < DataCount; i++)
		pMem[i] = dr.pMem[i];
}

template <class T>
TDataRoot<T>::~TDataRoot()
{
	if (MemType == MEM_HOLDER)
		delete[] pMem;
	pMem = nullptr;
}

template <class T>
bool TDataRoot<T>::IsEmpty() const
{
	return DataCount == 0;
}

template <class T>
bool TDataRoot<T>::IsFull() const
{
	return DataCount == MemSize;
}

template <class T>
void TDataRoot<T>::SetMem(void *p, int Size)
{
	if (MemType == MEM_HOLDER)
		delete pMem;
	pMem = (T*)p;
	MemSize = Size;
	MemType = MEM_RENTER;
}

#endif
