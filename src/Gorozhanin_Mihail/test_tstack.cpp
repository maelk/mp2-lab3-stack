#include "tstack.h"
#include <gtest/gtest.h>

TEST(TStack, cant_create_stack_with_negative_size)
{
	ASSERT_ANY_THROW(TStack<int>(-1));
}

TEST(TStack, null_size_stack_is_created_correctly)
{
	ASSERT_NO_THROW(TStack<int>(0));
}

TEST(TStack, can_create_stack_with_positive_length)
{
	ASSERT_NO_THROW(TStack<int>(10));
}

TEST(TStack, cant_create_stack_with_negative_mem_added)
{
	ASSERT_ANY_THROW(TStack<int>(10, -1));
}

TEST(TStack, can_create_copied_stack)
{
	TStack<int> st1(2);

	ASSERT_NO_THROW(TStack<int> st2 = st1);
}

TEST(TStack, copied_stack_is_equal_to_source_one)
{
	TStack<int> st1(2);

	st1.Put(1);
	st1.Put(2);

	TStack<int> st2 = st1;

	EXPECT_EQ(1, (st2.Get() == 2) && (st2.Get() == 1));
}

TEST(TStack, copied_stack_has_its_own_memory)
{
	TStack<int> st1(2);

	st1.Put(1);
	st1.Put(2);

	TStack<int> st2 = st1;

	st1.Get();

	EXPECT_EQ(2, st2.Get());
}

TEST(TStack, can_put_element_when_stack_isnt_full)
{
	TStack<int> st1(2);

	ASSERT_NO_THROW(st1.Put(1));
}

TEST(TSimpleStack, can_put_element_when_stack_is_full)
{
	TStack<int> st(2);

	st.Put(1);
	st.Put(2);

	ASSERT_NO_THROW(st.Put(3));
}

TEST(TSimpleStack, addMem_works_correctly)
{
	TStack<int> st(2);

	st.Put(1);
	st.Put(2);
	st.Put(3);

	EXPECT_EQ(1, (st.Get() == 3) && (st.Get() == 2) && (st.Get() == 1));
}

TEST(TStack, can_get_element_when_stack_isnt_empty)
{
	TStack<int> st(2);

	st.Put(2);

	ASSERT_NO_THROW(st.Get());
}

TEST(TStack, cant_get_element_when_stack_is_empty)
{
	TStack<int> st(2);

	ASSERT_ANY_THROW(st.Get());
}

TEST(TStack, put_and_get_works_correctly)
{
	TStack<int> st(1);

	st.Put(1);

	EXPECT_EQ(1, st.Get());
}

TEST(TStack, IsEmpty_works_correctly)
{
	TStack<int> st(1);

	EXPECT_EQ(1, st.IsEmpty());
}

TEST(TStack, IsFull_works_correctly)
{
	TStack<int> st(1);

	st.Put(1);

	EXPECT_EQ(1, st.IsFull());
}