#include <iostream>
#include "tstack.h"

using namespace std;

void main()
{
	TStack<int> st(20);

	try 
	{
		for (int i = 0; i < 20; i++)
			st.Put(i);
		cout << "Our test stack is" << endl;
		for (int i = 0; i <= 20; i++) 
			cout << st.Get() << endl;
	}
	catch (int a)
	{
		cout << "Error code:" << st.GetRetCode() << endl; 
	}
}
