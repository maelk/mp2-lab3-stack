#include "tstack.h"

#include <gtest/gtest.h>

TEST(TStack, can_create_stack)
{
	ASSERT_NO_THROW(TStack st());
}


TEST(TStack, new_stack_is_empty)
{
	TStack st;

	EXPECT_TRUE(st.IsEmpty());
}

TEST(TStack, stack_with_element_not_empty)
{
	TStack st;
	st.Put(1);

	EXPECT_FALSE(st.IsEmpty());
}

TEST(TStack, stack_after_get_operation_not_full)
{
	TStack st(2);
	st.Put(0);
	st.Put(1);
	st.Put(2);

	st.Get();

	EXPECT_FALSE(st.IsFull());
}

TEST(TStack, get_operation_take_top_element)
{
	TStack st(2);
	st.Put(0);
	st.Put(1);
	st.Put(2);

	EXPECT_EQ(2, st.Get());
}

TEST(TStack, throws_when_get_from_empty_stack)
{
	TStack st(2);

	ASSERT_ANY_THROW(st.Get());
}

TEST(TStack, can_create_copied_stack)
{
	TStack st(5);

	ASSERT_NO_THROW(TStack st1(st));
}

TEST(TStack, copied_stack_is_equal_to_source_one)
{
	const int size = 3;
	bool res = true;
	TStack st1(size);
	for (int i = 0; i < size; i++)
		st1.Put(1);
	TStack st2(st1);
	for (int i = 0; i < size; i++)
	if (st1.GetTopElem() != st2.GetTopElem())
		res = false;

	EXPECT_TRUE(res);
}

TEST(TStack, copied_stack_has_its_own_memory)
{
	const int size = 3;
	TStack st1(size);
	TStack st2(st1);

	EXPECT_NE(&st1, &st2);
}

TEST(TStack, can_put_in_full_stack)
{
	const int size = 3;
	TStack st(size);
	for (int i = 0; i < size; i++)
		st.Put(1);

	ASSERT_NO_THROW(st.Put(1));
}

TEST(TStack, gettopelem_show_top_element)
{
	const int size = 3;
	TStack st(size);
	for (int i = 0; i < size; i++)
		st.Put(1);

	EXPECT_EQ(1, st.GetTopElem());
}

TEST(TStack, gettopelem_not_take_element_from_stack)
{
	const int size = 3;
	TStack st(size);
	for (int i = 0; i < size; i++)
		st.Put(1);

	st.GetTopElem();

	EXPECT_TRUE(st.IsFull());
}

