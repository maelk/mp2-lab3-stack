#ifndef __AREXP_COMP_CPP__
#define __AREXP_COMP_CPP__

#include <iostream>
#include <string>
#include "tstack.h"
#include "tsimplestack.h"
#define MAX_STACK_AM 64

using namespace std;

// ��������� ������������ ����������� ������ � ���������. �������� ������� ������������ ������
int is_correct(string str) {

	TStack op_brackets(MAX_STACK_AM);
	int errors = 0, brackets_am = 1;

	for (string::iterator iter = str.begin(); iter != str.end(); ++iter) {
		if (*iter == '(') {
			op_brackets.Put(brackets_am++);
		} else if (*iter == ')') {
			if (op_brackets.IsEmpty()) {
				cout << "0 - " << brackets_am++ << endl;
				errors++;
			} else {
				cout << op_brackets.Get() << " - " << brackets_am++ << endl;
			}
		}
	}

	while (!op_brackets.IsEmpty()) {
		cout << op_brackets.Get() << " - 0" << endl;
		errors++;
	}

	cout << errors << " errors were found." << endl;
		
	return errors;

}

int get_priority(char symb) {
	switch (symb) {
		case '(':
			return 0;
		case ')':
			return 1;
		case '+':
		case '-':
			return 2;
		case '*':
		case '/':
			return 3;
	}

	return -1;
}
 
string convert_to_postfix_form(string str_inf) {

	TStack operations(MAX_STACK_AM);
	string str_postfix;
	int curr_priority, prev_priority;
	char curr_symb, temp_symb;
	bool work_with_operand;

	for (string::iterator iter = str_inf.begin(); iter != str_inf.end(); ++iter) {
		if (*iter == ' ') {
			str_inf.erase(iter);
		}
	}

	for (string::iterator iter = str_inf.begin(); iter != str_inf.end(); ++iter) {

		curr_priority = get_priority(*iter);
		curr_symb = *iter;

		// ���� ������ ������ - ����� ��������
		if (curr_priority == -1) {

			work_with_operand = true;
			str_postfix += curr_symb;

		// ���� ������ ������ - ��������
		} else {

			if (work_with_operand) {
				work_with_operand = false;
				str_postfix += ' ';
			}

			if (curr_symb == ')') {

				while (get_priority(temp_symb = operations.Get()) != 0) {
					str_postfix += temp_symb;
				}
				operations.Get(); // ������������ ����������� ������

			} else if (curr_priority == 0 || curr_priority > prev_priority || operations.IsEmpty()) {
					
				operations.Put(curr_symb);

			} else {

				while (get_priority(temp_symb = operations.Get()) >= curr_priority) {
					str_postfix += temp_symb;
				}
				str_postfix += ' ';
				operations.Put(curr_symb);

			}

		}

		prev_priority = curr_priority;

	} // for

	while (!operations.IsEmpty()) {
		str_postfix += operations.Get();
	}

	return str_postfix;
}

double compute(string str) {

	if (!is_correct(str)) return 0;

	str = convert_to_postfix_form(str);

	TSimpleStack<double, MAX_STACK_AM> operands;
	string temp;
	int curr_priority;
	double first_operand, sec_operand, res;

	for (string::iterator iter = str.begin(); iter != str.end(); ++iter) {

		curr_priority = get_priority(*iter);

		// ���� ������ ������ - ����� ��������
		if (curr_priority == -1 && *iter != ' ') {

			temp += *iter;

		} else {

			if (temp.length()) {
				operands.push(atof(temp.c_str()));
				temp = "";
			}

			if (curr_priority >= 2) {
				sec_operand = operands.pop();
				first_operand = operands.pop();

				switch (*iter) {
				case '+':
					res = first_operand + sec_operand;
					break;
				case '-':
					res = first_operand - sec_operand;
					break;
				case '*':
					res = first_operand * sec_operand;
					break;
				case '/':
					res = first_operand / sec_operand;
					break;
				}

				operands.push(res);
			}
		} // if-else
	} // for

	return res;

}

#endif