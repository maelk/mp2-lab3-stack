#ifndef __TSTACK_H__
#define __TSTACK_H__

#include "tdataroot.h"

class TStack: public TDataRoot {
private:
	int top;

public:
	TStack(int Size = DefMemSize): TDataRoot(Size), top(-1) {};
	void Put(const TData& Val);
	TData Get();
	void Print();
};

#endif